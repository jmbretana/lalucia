<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <title>La Lucia - La Naturaleza es suya, nosotros le damos el lugar</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- Google Tag Manager -->
  <script>(function (w, d, s, l, i) {
      w[l] = w[l] || []; w[l].push({
        'gtm.start':
          new Date().getTime(), event: 'gtm.js'
      }); var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
          'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PJMWT4G');</script>
  <!-- End Google Tag Manager -->
</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJMWT4G" height="0" width="0"
      style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!--==========================
  Header
  ============================-->
  <header id="header">
    <?php include_once('views/headerView.php'); ?>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->



  <main id="main">
    <section id="portfolio" class="section-bg">
      <div class="container containerGaleria">
        <!-- begin 60mts -->
        <h3 class="section-title"><i class="ion ion-camera"></i> Galería de Fotos</h3>

        <header class="gallery">
          <h4 class="section-title"><a href="galeria60.php">Departamentos 60mts</a></h4>
          <h4 class="section-title"><a href="galeria45e.php" class="active">Departamentos 45mts Estandar</a></h4>
          <h4 class="section-title"><a href="galeria45s.php">Departamentos 45mts Superior</a></h4>
          <h4 class="section-title"><a href="galeriaComplejo.php">El Complejo</a></h4>
        </header>

        <div class="row portfolio-container">
        <?php
          include_once('functions/db.php');

          try {
              $mysqli = conectar();
              $sql = "SELECT * FROM galeria WHERE categoria = 'cab_45_e '";

              if ($result = $mysqli -> query($sql)) {
                  while ($row = $result -> fetch_row()) {
                      $html = '
                        <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
                        <div class="portfolio-wrap">
                          <img src="'.$row[3].'" class="img-fluid" alt="">
                          <div class="portfolio-info">
                            <h4><a href="#">'.$row[1].'</a></h4>
                            <p>'.$row[2].'</p>
                            <div>
                              <a href="'.$row[3].'" class="link-preview" data-lightbox="portfolio"
                                data-title="'.$row[1].'" title="Preview"><i class="ion ion-eye"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>';

                      echo $html;
                  }

                  $result -> free_result();
              }
          } catch (PDOException $e) {
              echo 'Falló la conexión: ' . $e->getMessage();
          }
          
          ?>
        </div>
        <!-- end 45mts -->

      </div>
    </section><!-- #portfolio -->

    <!--
      ==========================
      Atracciones Section
    ============================
    -->
    <section id="atracciones" style="background-color: #eae3ff;">
      <div class="container">
        <header class="section-header">
          <h3 class="section-title"><i class="ion ion-navigate"></i> Donde estamos? </h3>
          <h4 class="text-center">San Clemente del Tuyú</h4>
        </header>

        <div class="row">
          <div style="margin: 0 auto; width: 90%;">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3211.992459333662!2d-56.71202688472176!3d-36.38517658003606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959c1b1c87dbb1c1%3A0x694949329f10188!2sCalle%2082%20155%2C%20San%20Clemente%20del%20Tuyu%2C%20Buenos%20Aires!5e0!3m2!1ses!2sar!4v1572363727226!5m2!1ses!2sar"
              frameborder="0" style="width: 100%; height: 400px; border:1px solid #ddd; border-radius: 10px;"
              allowfullscreen=""></iframe>
          </div>
        </div>

        <hr>

        <header class="section-header">
          <h4 class="text-center">Atracciones</h4>
        </header>

        <div class="row">
          <div class="col-md-3 col-lg-3">
            <div class=" box" style="padding: 0;">
              <div style="background: #ecebff;">
                <img src="img/mundo_marino.jpg" alt="Mundo Marino" style="width: 300px;">
              </div>
              <h4 class="title" style="padding: 10px 0 0 0;">Mundo Marino</h4>
            </div>
          </div>

          <div class="col-md-3 col-lg-3">
            <div class="box" style="padding: 0;">
              <div style="background: #fceef3;">
                <img src="img/termas_marinas.jpg" alt="Termas Marinas" style="width: 300px;">
              </div>
              <h4 class="title" style="padding: 10px 0 0 0;">Termas Marinas</h4>
            </div>
          </div>

          <div class="col-md-3 col-lg-3">
            <div class="box" style="padding: 0;">
              <div style="background: #fceef3;">
                <img src="img/parque_municipal.jpg" alt="Parque Municipal" style="width: 300px;">
              </div>
              <h4 class="title" style="padding: 10px 0 0 0;">Parque Municipal</h4>
            </div>
          </div>

          <div class="col-md-3 col-lg-3">
            <div class="box" style="padding: 0;">
              <div style="background: #fceef3;">
                <img src="img/kitesurf.jpg" alt="Kitesurf" style="width: 300px;">
              </div>
              <h4 class="title" style="padding: 10px 0 0 0;">Kite Surf - Punta Rasa</h4>
            </div>
          </div>
        </div>
      </div>
    </section><!-- #services -->
  </main>

  <!--==========================
      PROMO
    ============================-->
  <!--
  <section id="covid" class="wow" style="background-color: #5BB6AF; margin: 10px 0;">
    <div class="container">
      <div class="row">
        <div class="text-center">
          <h3 style="color: #fff; padding: 0;margin-top: 20px;"><strong>PROMO ENERO 10% </strong>reservando más
            de 10 días
          </h3>
        </div>
      </div>
    </div>
  </section>
  -->

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <?php include_once('views/footerView.php'); ?>  
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Button trigger modal -->
  <button type="button" class="btn back-whatsapp" id="enviarWhats">
    <i class="fa fa-whatsapp"></i> Contactanos
  </button>

  <!-- Modal Whatsapp -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-whatsapp"></i> Contactanos</h4>
        </div>
        <div class="modal-body">
          <label>Tu nombre:</label>
          <input type="text" id="textoWhats">
          <button type="button" class="btn btn-success" id="enviarWhats">Enviar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Uncomment below i you want to use a preloader -->
  <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/validate.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
  <script src="js/home.js"></script>

</body>

</html>