$(document).ready(function () {
  var body = $("#intro");

  var myModal = document.getElementById('myModal');
  var myInput = document.getElementById('myInput');

  myModal.addEventListener('shown.bs.modal', function () {
    myInput.focus();
  });

  var backgrounds = [
    "url(img/intro-bg.jpg)",
  ];
  var current = 0;

  function nextBackground() {
    current = ++current % backgrounds.length;
    $("#intro").css("background-image", backgrounds[current]);
    setTimeout(nextBackground, 0.5);
  }

  function telefonoAleatorio() {
    // Solo Sandra
    const arrayTelefono = ["541166050229"];

    // Solo Fabi
    //const arrayTelefono = ["541131123285"];

    // Aleatorio
    //const arrayTelefono = ["541166050229", "541131123285"];

    const telefono = Math.floor(Math.random() * arrayTelefono.length);

    return arrayTelefono[telefono];
  };

  $("#shortCutEnviarWhats").click(function (e) {
    var nombre = $("#textoWhats").val();

    var url =
      "https://api.whatsapp.com/send?phone=" +
      telefonoAleatorio() +
      "&text=Web: %20Hola!%20Te%20contacto%20desde%20la%20Web%20de%20La%20Lucia,%20me%20gustaría%20saber%20mas%20de%20los%20departamentos!";

    window.open(url, "_blank");
    $("#myModal").modal("hide");
  });

  $("#btnEnviar").click(function (e) {
    var datos =
      "name=" +
      $("#name").val() +
      "&cantidad=" +
      $("#cantidad").val() +
      "&email=" +
      $("#email").val() +
      "&desde=" +
      $("#desde").val() +
      "&hasta=" +
      $("#hasta").val() +
      "&message=" +
      $("#message").val();
    var validado = true;

    $("#nombreError").hide();
    $("#mailError").hide();
    $("#fechasError").hide();

    if ($("#name").val() == "") {
      $("#nombreError").show();
      validado = false;
    }

    if ($("#email").val() == "") {
      $("#mailError").show();
      validado = false;
    }

    if ($("#desde").val() > $("#hasta").val()) {
      $("#fechasError").show();
      validado = false;
    }

    if (validado) {
      jQuery.ajax({
        url: "enviarmail.php",
        data: datos,
        type: "POST",
        success: function (data) {
          $("#sendmessage").show();

          setTimeout(function () {
            $("#sendmessage").fadeOut(1000);
          }, 2000);

          $("#nombreError").hide();
          $("#mailError").hide();
          $("#fechasError").hide();

          $("#name").val("");
          $("#desde").val("");
          $("#hasta").val("");
          $("#email").val("");
          $("#message").val("");
          $("#cantidad").val("");
        },
        error: function () { },
      });
    }
  });

  $("#btnEnviarWhats").click(function (e) {
    var datos =
      "name=" +
      $("#name").val() +
      "&cantidad=" +
      $("#cantidad").val() +
      "&email=" +
      $("#email").val() +
      "&desde=" +
      $("#desde").val() +
      "&hasta=" +
      $("#hasta").val() +
      "&message=" +
      $("#message").val();
    var validado = true;

    $("#nombreError").hide();
    $("#mailError").hide();
    $("#fechasError").hide();

    if ($("#name").val() == "") {
      $("#nombreError").show();
      validado = false;
    }

    if ($("#email").val() == "") {
      $("#mailError").show();
      validado = false;
    }

    if ($("#desde").val() > $("#hasta").val()) {
      $("#fechasError").show();
      validado = false;
    }

    if (validado) {
      let text = "%20Hola!%20Te%20contacto%20desde%20la%20Web%20de%20La%20Lucia,%20me%20gustaría%20saber%20mas%20de%20los%20departamentos.";

      if ($("#cantidad").val() !== "" && $("#desde").val() !== "" && $("#hasta").val() !== "") {
        text = text + "%20Soy%20" + $("#name").val() + ", somos%20" + $("#cantidad").val() + "%20personas%20y%20estamos%20buscando%20para%20las%20fechas%20entre%20el%20" + $("#desde").val() + "%20al%20" + $("#hasta").val();
        text = text + ".%20Muchas%20Gracias!";
      }

      var url =
        "https://api.whatsapp.com/send?phone=" +
        telefonoAleatorio() +
        "&text=Web: " + text;

      window.open(url, "_blank");

      $("#sendmessage").show();

      setTimeout(function () {
        $("#sendmessage").fadeOut(1000);
      }, 2000);

      $("#nombreError").hide();
      $("#mailError").hide();
      $("#fechasError").hide();

      $("#name").val("");
      $("#desde").val("");
      $("#hasta").val("");
      $("#email").val("");
      $("#message").val("");
      $("#cantidad").val("");
    }
  });

});
