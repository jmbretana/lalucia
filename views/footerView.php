<div class="footer-top">
    <div class="container">
    <div class="row">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-sm-6">
            <div class="footer-info">
                <h3>La Lucia</h3>
                <p>La Naturaleza es suya, nosotros le damos el lugar.</p>
            </div>
            </div>

            <div class="col-sm-6">
            <div class="footer-links">
                <h4>Contactanos</h4>
                <p>
                <a href="https://goo.gl/maps/EEpanRzER4QDWJtLA" target="_blank">Calle 82 Nro. 155 <br>
                    San Clemente del Tuyú, <br>
                    Buenos Aires. Argentina</a><br>
                <br>
                <strong>Teléfonos:</strong><br>
                <i class="fa fa-phone"></i> <a href="tel:01166050229">011-6605-0229</a><br>
                <i class="fa fa-whatsapp"></i> <a href="#" class="enviarWhats" target="_blank">011-6605-0229</a><br>
                <br>
                <strong>Email:</strong> <a href="mailto:info@la-lucia.com.ar">info@la-lucia.com.ar</a><br>
                </p>
            </div>

            <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="https://www.instagram.com/complejolalucia/" target="_blank" class="instagram"><i
                    class="fa fa-instagram"></i></a>
                <a href="#" class="enviarWhats"><i class="fa fa-whatsapp"></i></a>
            </div>

            </div>

        </div>

        </div>

        <div class="col-lg-6">

        <div class="form">

            <h4>Contactanos</h4>
            <form action="#" method="post" class="contactForm" id="formularioContacto">
            <div class="form-group">
                <div class="row">
                <label class="col-sm-2 control-label" style="padding-top: 6px;">Desde</label>
                <div class="col-sm-4">
                    <input type="date" class="form-control" name="desde" id="desde" />
                </div>
                <label class="col-sm-2 control-label" style="padding-top: 6px;">Hasta</label>
                <div class="col-sm-4">
                    <input type="date" class="form-control" name="hasta" id="hasta" />
                </div>
                </div>
                <label id="fechasError" style="display: none; font-size: 0.8em; color: red;">La fecha desde debe ser
                menor a la fecha hasta</label>
            </div>

            <div class="form-group">

            </div>

            <div class="form-group">
                <div class="row">
                <div class="col-sm-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Tu nombre"
                    data-rule="minlen:4" data-msg="Por favor ingresa tu nombre" required />
                    <label id="nombreError" style="display: none; font-size: 0.8em; color: red;">Por favor indique su
                    nombre</label>
                </div>

                <label class="col-sm-4 control-label" style="padding-top: 6px;">Cant. de Personas</label>
                <div class="col-sm-2">
                    <input type="number" min="1" class="form-control" name="cantidad" id="cantidad" />
                </div>
                </div>
            </div>
            <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Tu Email"
                data-rule="email" data-msg="Por favor ingresa un email valido" />
                <label id="mailError" style="display: none; font-size: 0.8em; color: red;">Por favor indique su
                correo</label>
            </div>

            <div class="form-group">
                <textarea class="form-control" name="message" id="message" rows="5" data-rule="required"
                data-msg="Campo requerido" placeholder="Por favor ingresá tu consulta"></textarea>
            </div>

            <div id="sendmessage">Solicitud enviada. Muchas gracias! <i class="fa fa-smile-o"
                aria-hidden="true"></i></div>

            <div id="errormessage"></div>

            <div class="text-center">
                <button type="button" class="btn" title="Enviar" id="btnEnviar"><i class="fa fa-envelope"></i> Enviar</button>
                <button type="button" class="btn btn-success" title="Enviar" id="btnEnviarWhats"><i class="fa fa-whatsapp"></i> Enviar</button>
            </div>
            </form>
        </div>

        </div>

    </div>

    </div>
</div>

<div class="container">
    <div class="copyright">

    <div style="padding-bottom: 5px;"> &copy; Copyright <strong>La Lucia</strong>. All Rights Reserved</div>
    <div style="padding-bottom: 20px;font-size: 12px;"> Sitio web desarrollado por <a
        href="https://coudefly.com" target="_blank"><strong>Coudefly</strong></a>: <i>"We develop, you grow"</i></div>

    <span id="siteseal">
        <script async type="text/javascript"
        src="https://seal.godaddy.com/getSeal?sealID=s6wsyEEwL00n5IrISXBEpkbbIsNKvyW6YXOqHXDRa7yrI9MPO55AKAQr3kuI"></script>
    </span>
    </div>
</div>