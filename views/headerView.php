  <div id="topbar">
      <div class="container">
          <div class="social-links">
              <a href="https://www.instagram.com/complejolalucia/" target="_blank" class="instagram"
                  style="color: #fff;"><i class="fa fa-instagram"></i></a>
              <a href="#" style="color: #fff;" class="enviarWhats" id="enviarWhats"><i class="fa fa-whatsapp"></i></a>
          </div>
      </div>
  </div>

  <div class="container">
      <div class="logo float-left">
          <h1>La Lucia</h1>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
          <ul>
              <li><a href="index.php#intro">Home</a></li>
              <li><a href="index.php#about">Nosotros</a></li>
              <li><a href="index.php#services">Servicios</a></li>
              <li><a href="galeria60.php">Galería de Fotos</a></li>
              <li><a href="index.php#atracciones">San Clemente del Tuyú</a></li>
              <li><a href="#footer">Contactanos</a></li>
          </ul>
      </nav><!-- .main-nav -->
  </div>