<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>La Lucia - La Naturaleza es suya, nosotros le damos el lugar</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Google Tag Manager -->
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PJMWT4G');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJMWT4G" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!--==========================
  Header
  ============================-->
    <header id="header">
        <?php include_once('views/headerView.php'); ?>
    </header><!-- #header -->

    <!--==========================
    Intro Section
  ============================-->
    <section id="intro" class="clearfix homeBackground">
        <div class="container d-flex h-100">
            <div class="row justify-content-center align-self-center">
                <div class="col-md-8 intro-info order-md-first order-last">
                    <h2 style="font-size: 3em;">La Lucia</h2>
                    <h3 style="color: #fff;">La Naturaleza es suya, nosotros le damos el lugar.</h3>
                </div>

                <div class="col-md-4 intro-img order-md-last order-first">
                    <!-- <img src="img/intro-img.svg" alt="" class="img-fluid"> -->
                </div>
            </div>
        </div>
    </section><!-- #intro -->



    <main id="main">
        <!--==========================
      About Us Section
    ============================-->
        <section id="about">
            <div class="container">
                <div class="row">

                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/about-img.jpg" alt="">
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-6">
                        <div class="about-content">
                            <h2>Nosotros</h2>
                            <h3>San Clemente del Tuyu , el bosque y el Mar juntos en sintonía construyendo
                                un lugar único en donde vivir sin prisa…</h3>
                            <p style="text-align:justify;">Sus calles de arena, sus playas anchas y
                                solitarias su cielo indescriptible , son alguna de sus característica principales.
                                Nuestro complejo se levanta en este entorno, prevaleciendo en las mismas el
                                compromiso con la naturaleza.</p>
                            <p style="text-align: justify;">La característica principal, es que generosamente se
                                construyeron conservando mucho espacio verde entre las mismas, los que le otorga
                                privacidad y buen gusto.</p>
                        </div>
                    </div>
                </div>
            </div>

        </section><!-- #about -->

        <!--==========================
      Protocolo COVID Section
    ============================-->
        <section id="covid" class="wow">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 text-center text-lg-left">
                        <h3 class="cta-title">Protocolo COVID</h3>
                        <p class="cta-text" style="margin: 0;">Ingresando en ver mas podes conocer el detalle de temas a
                            tener en
                            cuenta</p>
                    </div>
                    <div class="col-lg-3 cta-btn-container text-center">
                        <a class="cta-btn align-middle" href="covid.php">Ver mas <i class="ion-ios-more"></i></a>
                    </div>
                </div>
            </div>
        </section><!-- #proyecto2021 -->

        <!--==========================
      Services Section
    ============================-->
        <section id="services" class="section-bg">
            <div class="container">

                <header class="section-header">
                    <h3>Servicios</h3>
                </header>

                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="box">
                            <div class="icon" style="background: #ecebff;"><i class="ion-ios-home-outline"
                                    style="color: #8660fe;"></i></div>
                            <h4 class="title" style="margin: 0;"><a href="">Departamentos de Mar 60 mts Duplex</a></h4>
                            <h5 class="title" style="font-size: 0.9em;padding: 0; font-weight: normal;"><a
                                    href="galeria60.php">Para
                                    4 ó 5
                                    Personas</a></h5>

                            <ul class="text-left">
                                <li>Amplio Estar con 1 - Cocina – Comedor</li>
                                <li>Toilette</li>
                                <li>Microondas</li>
                                <li>Heladera con Freezer</li>
                                <li>Batería de cocina completa</li>
                                <li>TV LCD en Comedor</li>
                                <li>Dormitorio</li>
                                <li>Dormitorio Principal Sommier - Colchón de alta densidad</li>
                                <li>Baño completo amplio</li>
                                <li>TV LCD en Dormitorio</li>
                                <li>Aire acondicionado </li>
                                <li>Terraza balcón</li>
                                <li>Wifi</li>
                                <li>Perimetro seguro</li>
                                <li>Cochera interna</li>
                                <li>Patio con parrilla privada - Mesas y sillas</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-6">
                        <div class="box">
                            <div class="icon" style="background: #fceef3;"><i class="ion-ios-home-outline"
                                    style="color: #ff689b;"></i></div>
                            <h4 class="title" style="margin: 0;"><a href="galeria45e.php">Departamentos de Mar 45 mts
                                    Estandar
                                    <br>(Planta baja)</a></h4>
                            <h5 class="title" style="font-size: 0.9em;padding: 0; font-weight: normal;"><a href="">Para
                                    2 ó 3
                                    Personas</a></h5>
                            <ul class="text-left">
                                <li>Amplio Estar con Cocina – Comedor con cama nido</li>
                                <li>Dormitorio Principal Muy amplio - Sommier con colchón de alta
                                    densidad</li>
                                <li>Aire Acondicionado</li>
                                <li>Baño completo</li>
                                <li>TV LCD</li>
                                <li>Microondas</li>
                                <li>Heladera con Freezer</li>
                                <li>Batería de cocina completa</li>
                                <li>Pava eléctrica</li>
                                <li>Tostadora</li>
                                <li>Wifi</li>
                                <li>Perimetro seguro</li>
                                <li>Cochera interna</li>
                                <li>Patio con Parrilla privado- Mesa y sillas</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-6">
                        <div class="box">
                            <div class="icon" style="background: #fceef3;"><i class="ion-ios-home-outline"
                                    style="color: #ff689b;"></i></div>
                            <h4 class="title" style="margin: 0;"><a href="galeri45s.php">Departamentos de Mar 45 mts
                                    Superior <br>
                                    (Planta
                                    Alta)</a></h4>
                            <h5 class="title" style="font-size: 0.9em;padding: 0; font-weight: normal;"><a href="">Para
                                    2 ó 3
                                    Personas</a></h5>
                            <ul class="text-left">
                                <li>Amplio Estar con Cocina – Comedor con cama nido</li>
                                <li>Dormitorio Principal Muy amplio - Sommier con colchón de alta
                                    densidad</li>
                                <li>Aire Acondicionado</li>
                                <li>Baño completo</li>
                                <li>TV LCD</li>
                                <li>Microondas</li>
                                <li>Heladera con Freezer</li>
                                <li>Batería de cocina completa</li>
                                <li>Pava eléctrica</li>
                                <li>Tostadora</li>
                                <li>Wifi</li>
                                <li>Perimetro seguro</li>
                                <li>Cochera interna</li>
                                <li>Deck de madera con mesas y sillas- Parrilla individual</li>
                                <li>Vista lateral al mar</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- #services -->

        <!--==========================
      Call To Action Section
    ============================-->
        <section id="call-to-action">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 text-center text-lg-left">
                        <h3 class="cta-title">Contactanos</h3>
                        <p class="cta-text"> Envianos tu consulta por fechas y disponibilidad</p>
                    </div>
                    <div class="col-lg-3 cta-btn-container text-center">
                        <a class="cta-btn align-middle" href="#footer">Contactanos</a>
                    </div>
                </div>

            </div>
        </section><!-- #call-to-action -->

        <!--
      ==========================
      Atracciones Section
    ============================
    -->
        <section id="atracciones" style="background-color: #eae3ff;">
            <div class="container">
                <header class="section-header">
                    <h3 class="section-title"><i class="ion ion-navigate"></i> Donde estamos? </h3>
                    <h4 class="text-center">San Clemente del Tuyú</h4>
                </header>

                <div class="row">
                    <div style="margin: 0 auto; width: 90%;">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3211.992459333662!2d-56.71202688472176!3d-36.38517658003606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959c1b1c87dbb1c1%3A0x694949329f10188!2sCalle%2082%20155%2C%20San%20Clemente%20del%20Tuyu%2C%20Buenos%20Aires!5e0!3m2!1ses!2sar!4v1572363727226!5m2!1ses!2sar"
                            frameborder="0"
                            style="width: 100%; height: 400px; border:1px solid #ddd; border-radius: 10px;"
                            allowfullscreen=""></iframe>
                    </div>
                </div>

                <hr>

                <header class="section-header">
                    <h4 class="text-center">Atracciones</h4>
                </header>

                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div class=" box" style="padding: 0;">
                            <div style="background: #ecebff;">
                                <img src="img/mundo_marino.jpg" alt="Mundo Marino" style="width: 300px;">
                            </div>
                            <h4 class="title" style="padding: 10px 0 0 0;">Mundo Marino</h4>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="box" style="padding: 0;">
                            <div style="background: #fceef3;">
                                <img src="img/termas_marinas.jpg" alt="Termas Marinas" style="width: 300px;">
                            </div>
                            <h4 class="title" style="padding: 10px 0 0 0;">Termas Marinas</h4>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="box" style="padding: 0;">
                            <div style="background: #fceef3;">
                                <img src="img/parque_municipal.jpg" alt="Parque Municipal" style="width: 300px;">
                            </div>
                            <h4 class="title" style="padding: 10px 0 0 0;">Parque Municipal</h4>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="box" style="padding: 0;">
                            <div style="background: #fceef3;">
                                <img src="img/kitesurf.jpg" alt="Kitesurf" style="width: 300px;">
                            </div>
                            <h4 class="title" style="padding: 10px 0 0 0;">Kite Surf - Punta Rasa</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- #services -->
    </main>

    <!--==========================
      PROMO
    ============================-->
    <!--
  <section id="covid" class="wow" style="background-color: #5BB6AF; margin: 10px 0;">
    <div class="container">
      <div class="row">
        <div class="text-center">
          <h3 style="color: #fff; padding: 0;margin-top: 20px;"><strong>PROMO ENERO 10% </strong>reservando más
            de 10 días
          </h3>
        </div>
      </div>
    </div>
  </section>
  -->

    <!--==========================
    Footer
  ============================-->
    <footer id="footer">
        <?php include_once('views/footerView.php'); ?>
    </footer><!-- #footer -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- Button trigger modal -->
    <button type="button" class="btn back-whatsapp" id="shortCutEnviarWhats">
        <i class="fa fa-whatsapp"></i> Contactanos
    </button>

    <!-- Modal Whatsapp -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-whatsapp"></i> Contactanos</h4>
                </div>
                <div class="modal-body">
                    <label>Tu nombre:</label>
                    <input type="text" id="textoWhats">
                    <button type="button" class="btn btn-success" id="enviarWhats">Enviar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Uncomment below i you want to use a preloader -->
    <div id="preloader"></div>

    <!-- JavaScript Libraries -->
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/validate.js"></script>
    <script src="lib/jquery/jquery-migrate.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/mobile-nav/mobile-nav.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/isotope/isotope.pkgd.min.js"></script>
    <script src="lib/lightbox/js/lightbox.min.js"></script>

    <!-- Contact Form JavaScript File -->
    <script src="contactform/contactform.js"></script>

    <!-- Template Main Javascript File -->
    <script src="js/main.js"></script>
    <script src="js/home.js"></script>

</body>

</html>
