<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <title>La Lucia - La Naturaleza es suya, nosotros le damos el lugar</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="La Lucia, san clemente, " name="keywords" />
  <meta content="Cabañas en San Clemente del Tuyú. La Naturaleza es suya, nosotros le damos el lugar."
    name="description" />

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon" />

  <!-- Google Fonts -->
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700"
    rel="stylesheet" />

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
  <link href="lib/animate/animate.min.css" rel="stylesheet" />
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet" />
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" />
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet" />

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet" />

  <!-- Google Tag Manager -->
  <script>
    (function (w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({ "gtm.start": new Date().getTime(), event: "gtm.js" });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != "dataLayer" ? "&l=" + l : "";
      j.async = true;
      j.src = "https://www.googletagmanager.com/gtm.js?id=" + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, "script", "dataLayer", "GTM-PJMWT4G");
  </script>
  <!-- End Google Tag Manager -->
</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJMWT4G" height="0" width="0"
      style="display: none; visibility: hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!--==========================
  Header
  ============================-->
  <header id="header">
    <?php include_once('views/headerView.php'); ?>
  </header><!-- #header -->

  <!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix homeBackground">
  <section id="intro" class="clearfix ">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center">
        <div class="col-md-8 intro-info order-md-first order-last">
          <h2 style="font-size: 3em; color: #fff">Covid-19</h2>
          <h3 style="color: #fff">
            Te detallamos nuestro protocolo en La Lucia respecto al COVID-19
          </h3>
        </div>

        <div class="col-md-4 intro-img order-md-last order-first"></div>
      </div>
    </div>
  </section>
  <!-- #intro -->

  <main id="main">
    <section id="portfolio" class="section-bg">
      <div class="container">
        <div class="row portfolio-container">
          <div class="col-lg-12 col-md-12 portfolio-item filter-app" style="background-color: #fff; padding: 30px">
            <p>
              <strong>Covid-19</strong> temas importantes a tener en cuenta
              ante la pandemia EN CASO DE PRESENTAR SÍNTOMAS INFORMAR DE
              INMEDIATO
            </p>

            <ul>
              <li>
                EN CASO DE PRESENTAR SÍNTOMAS SE DEBE DEJAR EL ESTABLECIMIENTO
                A LA BREVEDAD, POR SUS PROPIOS MEDIOS Y REGRESAR A SU LUGAR DE
                ORIGEN
              </li>
              <li>SE DEBERA USAR BARBIJO PARA CIRCULAR POR EL COMPLEJO</li>
              <li>NO ESTAN PERMITIDAS LAS VISITAS.</li>
              <li>
                EL COMPLEJO NO SE HARÁ RESPONSABLE DEL TRASLADO, NI DE NINGÚN
                TIPO DE TRATAMIENTO.
              </li>
              <li>TODAS LAS CABAÑAS CONTARÁN CON SANITIZANTES</li>
              <li>
                EL COMPLEJO SE HARÁ RESPONSABLE DE LA HIGIENE DEL PARQUE Y LOS
                DEPARTAMENTOS ESTARAN EN CONDICIONES SANITARIAS OPTIMAS.
              </li>
              <li>LOS SILLONES Y MESAS ESTARÁN NUMERADOS POR CABAÑA</li>
              <li>NO SE PROVEERÁ BLANQUERIA</li>
              <li>
                SE REALIZARÁ LA LIMPIEZA GENERAL DE LOS DEPARTAMENTOS,
                DESINFECCIÓN Y LIMPIEZA DE FILTROS DE AIRE ACONDICIONADO ENTRE
                HUÉSPEDES.
              </li>
              <li>
                LOS CONTROLES REMOTOS SERÁN ENTREGADOS SELLADOS Y
                DESINFECTADOS.
              </li>
              <li>
                LA SEÑA SERÁ DEL VALOR DE UNA NOCHE, y EN CASO DE PRESENTAR
                SÍNTOMAS Y NO PUEDES ASISTIR SE PUEDE CAMBIAR PARA OTRO
                MOMENTO, NO SE REALIZARÁ LA DEVOLUCIÓN DE LA MISMA.
              </li>
              <li>
                EN CASO DE PRESENTAR SÍNTOMAS Y TENER QUE RETIRARSE ANTES DE
                FINALIZAR SU RESERVA, EL COMPLEJO NO HARA DEVOLUCION DEL
                DINERO.
              </li>
              <li>
                APELAMOS AL CUIDADO ENTRE TODOS, EL RESPETO, Y LA SOLIDARIDAD
                PARA QUE PUEDAN DISFRUTAR DE SU ESTADÍA Y SUS VACACIONES
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- #portfolio -->
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <?php include_once('views/footerView.php'); ?>  
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <a href="https://api.whatsapp.com/send?phone=541166050229&text=Hola!%20Me%20gustaria%20saber%20mas%20de%20las%20cabañas!"
    target="_blank" class="back-whatsapp"><i class="fa fa-whatsapp"></i> Contactanos</a>

  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

  <script>
    $(document).ready(function () {
      $("#btnEnviar").click(function (e) {
        var datos =
          "name=" +
          $("#name").val() +
          "&email=" +
          $("#email").val() +
          "&message=" +
          $("#message").val();

        jQuery.ajax({
          url: "enviarmail.php",
          data: datos,
          type: "POST",
          success: function (data) {
            $("#sendmessage").show();

            setTimeout(function () {
              $("#sendmessage").fadeOut(1000);
            }, 2000);

            $("#name").val("");
            $("#email").val("");
            $("#message").val("");
          },
          error: function () { },
        });
      });
    });
  </script>
</body>

</html>