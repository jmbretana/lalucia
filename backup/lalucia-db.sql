-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 01, 2022 at 06:02 AM
-- Server version: 5.6.51-cll-lve
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lalucia-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `fecha_desde` varchar(50) NOT NULL,
  `fecha_hasta` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cantidad` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `consulta` varchar(1000) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacto`
--

INSERT INTO `contacto` (`id`, `fecha_desde`, `fecha_hasta`, `nombre`, `cantidad`, `email`, `consulta`, `fecha_registro`) VALUES
(4, '2021-01-22', '2021-01-25', 'Ximena Milla', '5', 'xime.a.milla@gmail.com', 'Hola, cuales son los precios por día en Enero para 5 personas?\n\nGracias!', '2021-01-19 01:19:47'),
(5, '', '', 'Veronica', '', 'vero_orona1204@hotmail.com', 'Hola! Como estan? Queria saber si cuentan con disponibilidad desde el 13 al 20 de febrero, para 2 personas\nSaludos', '2021-01-19 13:09:47'),
(6, '2021-01-20', '2021-01-26', 'Grey Quiroga ', '3', 'grey169@hotmail.com', 'Buenos dias quiero saber disponibilidad y precio para estas fechas somos dos adultos y un bebe de un año. Gracias ', '2021-01-19 13:36:46'),
(7, '2021-02-04', '2021-02-07', 'Jose', '2', 'dptolegalconcejomunicipal@gmail.com', 'Aceptan tarjeta de Crédito?', '2021-01-20 00:21:27'),
(8, '2021-01-19', '2021-01-21', 'Marto', '4', 'jmbretana@gmail.com', 'Prueba de marto', '2021-01-20 18:41:47'),
(9, '2021-01-21', '2021-01-25', 'Juan Carlos Enrique Freire', '', 'piratabarrow@hotmail.com', 'Hola, quiero saber ubicación de los duplex, si es del lado el vivero o del lado de las termas y si puedo llevar una mascota, perro chico.', '2021-01-20 18:53:49'),
(10, '2021-02-20', '2021-02-23', 'Fabian Walter ', '4', 'fabmiguenz@gmail.com', 'Quisiera saber cuanto sale la cabaña para 4 personas por noche. Muchas gracias..', '2021-01-21 15:27:03'),
(11, '2021-02-01', '2021-02-07', 'Sabrina', '4', 'sgbarret@gmail.com', 'Quisiera saber si tienen disponibilidad y en tal caso, el precio de la misma. Gracias.', '2021-01-22 12:39:09'),
(12, '2021-02-13', '2021-02-20', 'Patricia', '2', 'patolozicki@hotmail.com', 'Buenos dias, quisiera consultar precio y disponibilidad para un departamento para 2 adultos con una mascota (bulldog frances) del 13/2 al 20/2. Gracias.', '2021-01-22 14:05:09'),
(13, '2021-01-29', '2021-02-04', 'carina', '3', 'carinaher8@gmail.com', 'hola, quería saber si tenían disponibilidad en las fechas solicitadas y el costo. somos dos adultas, una menor de 5 años y una caniche mini toy adulta, de 6 años, no ladra, no pierde pelo y que se la pasa durmiendo :) Muchas gracias', '2021-01-23 04:10:39'),
(14, '2021-01-23', '2021-01-28', 'Florencia ', '3', 'estudio_martino@hotmail.com', 'Hola quería saber el valor por día en el duplex de 60 mts en enero. Y si son en planta alta . Saludos ', '2021-01-23 23:33:09'),
(15, '2021-01-27', '2021-01-29', 'mauro', '5', 'maurogve@yahoo.com.ar', 'Hola, disponibilidad y precio? Aceptan mascotas (caniche muy pequeño)', '2021-01-24 23:45:07'),
(16, '2021-02-12', '2021-02-15', 'Mariela', '2', 'mariguag@hotmail.com', 'Quisiera saber disponibilidad y precio.\nGracias!', '2021-01-25 00:04:06'),
(17, '2021-02-08', '2021-02-14', 'Rocio ', '4', 'rorcioadiaz@hotmail.com', 'Que tal. Necesitaría información...\nPrecio por siete días del 8 de febrero al 14 de febrero.  2 adultos y 2 mejores. Gracia ', '2021-01-25 02:25:36'),
(18, '2021-02-20', '2021-02-28', 'Paola', '4', 'pr2254626@gmail.com', 'Hola cuanto cuesta el alquiler', '2021-01-25 02:41:54'),
(19, '2021-02-12', '2021-02-17', 'Gabriela Hang', '2', 'ferheredia_lpda@hotmail.com', 'Precio cabaña 2 personas, cerca del mar ', '2021-01-25 13:01:11'),
(20, '2021-03-20', '2021-03-28', 'Karina', '5', 'karinaleiss.95@hotmail.com', 'Hola buenas tardes , me pasó su contacto Romina  , y quería saber el precio de la semana de marzo , son 8 días , del 20 al 28 ... Somos 5 adultos y un bebé menor ', '2021-01-25 15:27:37'),
(21, '2021-02-13', '2021-02-17', 'MONICA', '4', 'monykmalu@hotmail.com', 'BUENAS TARDES ,SOMOS DOS ADULTOS Y DOS NIÑOS ,QUISIERA SABER DISPONIBILIDAD Y PRECIO? GRACIAS', '2021-01-25 19:15:07'),
(22, '2021-02-01', '2021-02-07', 'Ana Lucia Alvarez', '3', 'alvarez.analucia@gmail.com', 'Queria consultar disponibilidad de una cabaña para 2 adultos y una niña de 3 años desde el 30/01/2021 al 07/02/2021, como asi también las tarifas.\n\nMuchas gracias.\nSaludos.', '2021-01-25 19:20:42'),
(23, '2021-02-13', '2021-02-16', 'Francisco Gimenez', '2', 'franci_max@hotmail.com', 'hola , para consultar precios por dia para una pareja en vuestro lugar.Gracias.', '2021-01-26 02:53:09'),
(24, '2021-02-23', '2021-02-28', 'Maria Paz', '2', 'mari.paz.borovik@gmail.com', 'Buen dia! Quería solicitar tarifas y medios de pago actualizados, también modo de reserva. Muchas gracias!', '2021-01-26 13:29:40'),
(25, '2021-02-05', '2021-02-10', 'Amalia', '2', 'garonoe8@gmail.com', 'Consulto por disponibilidad y tarifas para ese periodo\ngracias', '2021-01-26 13:58:26'),
(26, '2021-02-11', '2021-02-16', 'Emiliano ', '2', 'cambiaggioemiliano@gmail.com', 'Estimados buenas noches. Les escribo para saber disponibilidad y tarifas de las distintas cabañas para 2 personas en las fechas indicadas. Desde ya muchas gracias. Saludos.', '2021-01-27 00:46:01'),
(27, '2021-03-01', '2021-03-13', 'Oscar Maldonado', '2', 'oscarmaldonado@live.com.ar', 'Buen dia:\nQueriamos consultar precio de estadia en la fecha indicada para dos personas.\nDesde ya muchas gracias.\nSaludos', '2021-01-27 13:28:09'),
(28, '', '', 'Susana Beatriz Skorpanich', '', 'sskorpanich@gmail.com', 'Hospedaje del 11 al 16/02/2021 -- 2 personas', '2021-01-28 03:25:49'),
(29, '2021-02-05', '2021-02-10', 'Susana', '2', 'orgsalta@outlook.com', 'Alojamiento para un matrimonio', '2021-01-29 13:39:59'),
(30, '2021-02-26', '2021-03-01', 'Vanesa', '2', 'vane_77_05@hotmail.com', 'Hola queria saber si tenias disponible para la fecha marcada la tarifa y si aceptan mascotas?una perrita chica re tranquila', '2021-01-29 18:36:09'),
(31, '2021-02-02', '2021-02-07', 'leticia lana', '4', 'leticialana@hotmail.com', 'Tarifa y disponibilidad?\nSalduos\nLeticia', '2021-01-30 14:43:41'),
(32, '', '', 'isabel morini', '', 'isabel_morini@hotmail.com', 'buenas tardes ,principalmente quisiera saber si aceptan pequeñas mascotas ', '2021-01-30 17:34:01'),
(33, '2021-02-12', '2021-02-15', 'Marcela', '4', 'marcelacalderon1968@gmail.com', 'Quisiera saber valores . Horarios de ingreso y salida. Aceptan mascotas? Gracias ', '2021-01-30 18:07:42'),
(34, '2021-02-12', '2021-02-15', 'Marcela', '4', 'marcelacalderon1968@gmail.com', 'Quisiera saber valores . Horarios de ingreso y salida. Aceptan mascotas? Gracias ', '2021-01-30 18:07:44'),
(35, '2021-02-08', '2021-02-14', 'Liliana', '5', 'liliana_c4@hotmail.com', 'Buenas tardes, somos una familia de 3 adultos, una niña de 2 y uno de 13años, quisieramos saber si tiene disponibilad. Que servicios incluye. Muchas gracias', '2021-01-30 21:04:19'),
(36, '2021-02-12', '2021-02-17', 'Veronica', '4', 'veromarch.vm@gmail.com', 'Disponibilidad para esa fecha y formas de pago. Gracias!!', '2021-01-31 16:55:57'),
(37, '2021-02-04', '2021-02-09', 'valeria gandini', '5', 'valegandini@gmail.com', 'Hola buen dia, somos 2 adultos y 3 niñas de 2,8 y 11 años\nQueriamos saber si tienen disponibilidad para esas fechas.\nO a partir de que fechas, gracias!\nLa idea es irnos solo 4 / 5 dias nomas', '2021-02-01 12:53:50'),
(38, '2021-02-14', '2021-02-16', 'Stefania', '3', 'stefaniavilar_5@hotmail.com', 'Buenas Noches quiero info para 2 mayores y 1 menos de 8 años!\nGracias', '2021-02-02 01:55:24'),
(39, '2021-02-12', '2021-02-16', 'marcelo rondolino', '-5', 'mrondolino@intramed.net', 'que valor por la estadia y disponibilidad.para ese fin de semana.\n\nsdos.\nmarcelo', '2021-02-02 02:27:30'),
(40, '2021-02-13', '2021-02-16', 'SANDRA LOPEZ', '4', 'sandralopezgue@hotmail.com', 'Disponibilidad y tarifa. Cuatro adultos y un niño de 2 años', '2021-02-02 12:00:53'),
(41, '2021-02-06', '2021-02-10', 'silvina gomez', '2', 'pilirpa16can@hotmail.com', 'me gustaría saber si tienen disponibilidad para esa fecha somos un matrimonio con un nene de tres años', '2021-02-02 18:56:28'),
(42, '2021-02-17', '2021-02-20', 'Mariana Cebrero', '4', 'mcprensaycomunicacion@gmail.com', 'Buenas tardes, quería saber si el proyecto esta terminado. Si es así somos un matrimonio con dos nenas de 9 y 11 años. Quería saber disponilbilidad para tres o cuatro días dentro de la semana del 16 de febrero y costos. Muchas gracias', '2021-02-02 22:57:31'),
(43, '2021-02-15', '2021-02-20', 'Mabel Palomeque', '2', 'mabelpalomeque1793@gmail.com', 'Buenas noches, estoy interesada en un departamento para dos personas, quisera saber si cuentan con disponibilidad en ésas fechas y el costo por noche. Además, me gustaría que me envíe  fptos del lugar si es posible y que me confirme la ubicación  (cuadras de la playa y centro).\nMuchas gracias, saludos!', '2021-02-02 23:34:50'),
(44, '2021-02-04', '2021-02-07', 'Mariangeles', '4', 'mariangelesbertelli@yahoo.com.ar', 'Buen día quiero consultar disponibilidad y precio.Somos 4 personas.', '2021-02-03 10:51:35'),
(45, '2021-02-12', '2021-02-16', 'graciela', '-4', 'gestoriasalinas@hotmail.com', 'hola buen dia necesitaria si en los dias declarados tienen disponibilidad de cabañas, precio y servicios.gracias', '2021-02-03 13:25:56'),
(46, '2021-02-05', '2021-02-09', 'Susana', '2', 'orgsalta@outlook.com', 'Hola por alojamiento para matrimonio mayor del 05/02 al 09/02. Gracias', '2021-02-03 14:47:30'),
(47, '2021-02-19', '2021-02-21', 'karina', '8', 'ka.gioseffi@gmail.com', 'hola, buenas tardes! quisiera consultar por disponibilidad de 2 cabañas, ya que somos 2 familias las que iríamos, 2 adultos mas 2 niños pequeños por cabaña, y el precio por el fin de semana\ndesde ya muchas gracias!\nsaludos', '2021-02-03 20:20:44'),
(48, '2021-02-17', '2021-02-20', 'Diego', '2', 'josediegop@yahoo.com.ar', 'Reserva para 2 mayores y 1 nena de 6 años', '2021-02-03 23:12:55'),
(49, '2021-02-13', '2021-02-20', 'Mariano', '6', 'mariano135@gmail.com', 'Buenos días, quisiera saber valores para estadía en las fechas indicadas. De las 6 personas son 4 adultos y 2 menores (3 y 9 años). Adicionalmente quisiera saber si están permitidas las mascotas y bajo que condiciones.\nGracias.', '2021-02-04 15:43:15'),
(50, '2021-02-26', '2021-03-01', 'Emilce', '3', 'emi.villalba1@gmail.com', 'Hola que tal quería saber cuanto me saldría hospedarme esos días llegaría al lugar a las 6 am del 26 y me iría a las 21pm del 1. Somos 2 adultos y un nene de 4', '2021-02-05 17:19:27'),
(51, '2021-02-18', '2021-02-21', 'Adriana ', '3', 'mca1926@hotmail.com', 'Hola , queria saber acerca del lugar, somos dos adultos y un menor.\nSi tienen disponibilidad y cuanto cuesta.\n', '2021-02-05 18:12:46'),
(52, '2021-02-12', '2021-02-16', 'Marisol Goicoechea', '2', 'maris.goicoechea@hotmail.com', 'Buenas noches, quisiera que me brinden tarifas tanto para 2 personas como para 4 y si cuentan con disponibilidad para el fin de semana de carnaval. Gracias.', '2021-02-06 02:36:38'),
(53, '2021-02-13', '2021-02-16', 'Silvia', '6', 'silvia_preis@hotmail.com', 'disponibilidad . casa departamento  para 6 adultos un bebe un niño de 4', '2021-02-07 19:37:52'),
(54, '2021-02-14', '2021-02-15', 'Valeria Rosales ', '5', 'valeria15rosales@gmail.com', ' Busco alojamiento para 1 matrimonioy 3 adolescentes. Para los dias 14 y 15 de febrero', '2021-02-09 02:20:54'),
(55, '2021-02-13', '2021-02-16', 'Nancy', '', 'nancabral@hotmail.com', 'buen día, les agradeceré si me pueden decir si tienen disponible cabaña para 5 personas y costo', '2021-02-09 13:34:08'),
(56, '2021-02-11', '2021-02-14', 'Jorge ', '4', 'Jhacosta0228@gmail.com', 'Disponibilidad ', '2021-02-09 18:42:43'),
(57, '2021-02-22', '2021-02-25', 'Lucrecia', '3', 'lucrefla00@hotmail.com', 'Hola quería consultar la tarifa de un alojamiento para 2 mayores y un niño de 3 años', '2021-02-11 00:14:17'),
(58, '2021-02-13', '2021-02-16', 'Irma', '5', 'morenabianca86@gmail.com', 'Hola buenas noches quería averiguar el costo por 3 noches el horario de ingreso y a cuánto está del centro', '2021-02-11 00:41:32'),
(59, '2021-02-13', '2021-02-16', 'PABLO ARIEL GONZÁLEZ', '2', 'estudiopenal@yahoo.com.ar', 'Buenas noches. Quería consultar disponibilidad de alojamiento para dos personas adultas, desde el sábado 13 de febrero hasta el día martes 16 por la mañana. Muchas gracias.', '2021-02-11 03:54:42'),
(60, '2021-03-01', '2021-03-05', 'Sandra', '2', 'sanyzarate@yahoo.com.ar', 'Buenos días consulto por la estadía de 5 días. Viajamos con una perrita. También me gustaría saber si hay comercios cerca. Gracias', '2021-02-11 11:03:14'),
(61, '2021-02-13', '2021-02-16', 'Milagros', '2', 'milagrosburgos4@gmail.com', 'buenas noches! queria saber precios. gracias', '2021-02-12 00:37:28'),
(62, '2021-02-14', '2021-02-25', 'Rosana', '4', 'catalanalvarega@gmail.com', 'Hola, quería saber si tienen disponible para esa fecha y cuánto están cobrando, 2 mayores y 2 menores', '2021-02-12 10:33:49'),
(63, '2021-02-25', '2021-02-28', 'MARIA EUGENIA CAZOT', '2', 'eugecazot@gmail.com', 'Buenas tares, quisiera saber precio y disponibilidad.\nMuchas gracias, Eugenia.', '2021-02-14 18:11:48'),
(64, '2021-03-31', '2021-04-04', 'Maria Fernanda Umpierrez', '2', 'umpierrezmf@gmail.com', 'Quisiera saber el precio de estadía para un adulto y un menor, ingresando el miércoles 31 de marzo, hasta el domingo 4 de abril. \nGracias', '2021-02-14 22:39:41'),
(65, '2021-02-26', '2021-03-01', 'Lucrecia roude', '3', 'lucrefla00@hotmail.com', 'Buenos días quería saber si tenés disponibilidad desde el viernes 26 a el lunes 1/3. Y costo para 2 adultos y un niño de 3 años. Muchas gracias', '2021-02-15 12:56:49'),
(66, '', '', 'DIANA', '', 'dianaelenalazar@hotmail.com', 'somos dos personas PB, camas separadas del 2 de marzo al 7 de marzo cuanto nos saldria   GRACIAS', '2021-02-16 23:11:13'),
(67, '2021-02-21', '2021-02-26', 'Valeria', '4', 'vachu13@gmail.com', 'Que tal quisiera cosnultar el precio para esos dias.\nMucha gracais ', '2021-02-17 03:50:16'),
(68, '2021-02-21', '2021-02-24', 'sole ', '2', 'solecordoba67@gmail.com', 'Hola como esta? Quisiera saber los precios por noche', '2021-02-17 10:39:31'),
(69, '2021-02-22', '2021-03-01', 'Mariana', '3', 'marianpitra_79@hotmail.com', 'Disponibilidad y costo por 7 noches\nCon opción a 4 personas.', '2021-02-17 17:58:25'),
(70, '2021-02-28', '2021-03-06', 'Luana Dos Santos', '1', 'luana2santos@gmail.com', 'Quisiera saber cuál es el precio para estadía en depto standard en las fechas indicadas, y sí se admiten mascotas. Gracias!', '2021-02-20 18:17:25'),
(71, '2021-02-21', '2021-02-25', 'KARINA RAMIREZ', '4', 'karifabi_72@hotmail.com', 'tenes disponibilidad para esa fecha, precio?', '2021-02-20 22:27:04'),
(72, '2021-02-22', '2021-02-26', 'laura alvarez', '3', 'alvarezlaura.isabel@gmail.com', 'admiten mascotas?y si tienen disponibilidad y tarifas.\ngracias', '2021-02-21 18:50:23'),
(73, '2021-03-05', '2021-03-07', 'Lucas Troillet', '4', 'lucas_troillet@yahoo.com.ar', 'Buenas tardes, queria consultar disponibilidad para las fechas indicadas. Somos 2 adultos y 2 niños pequeños (2 y 6 años)\n\nGracias\n\nLucas', '2021-02-22 18:47:33'),
(74, '2021-03-04', '2021-03-11', 'Ruben zubieta', '2', 'pipozubi@hotmail.com', 'Precio por los 7 días y si hay que llevar sábanas y toallas', '2021-02-24 16:12:45'),
(75, '2021-03-20', '2021-03-28', 'Jorge Alberto Fernandez Ares', '2', 'jafares1329@gmail.com', 'Somos una pareja y quisiéramos saber disponibilidad y precio. Estaríamos llegando el sábado 20 de marzo y nuestra intención sería quedarnos hasta el domingo 28 de Marzo a la tarde. Es necesario llevar sábanas y/o toallas? Muchas gracias!! Jorge Ares', '2021-02-24 20:28:57'),
(76, '2021-03-07', '2021-03-10', 'Micaela Ruiz Diaz', '2', 'ruizdiazmicaela5@gmail.com', 'Hola quería saber si tenes disponibilidad para esas fechas y cual es el valor y horarios de ingreso y egreso. Gracias', '2021-02-25 17:13:26'),
(77, '2021-02-26', '2021-02-28', 'Fabian ', '4', 'fmaleichuk@gmail.com', 'Buenas tardes, tenes disponibilidad, para este fin de semana, llegariamos alrededor de las 18hs, si aceptan mascotas, y el costo del mismo.', '2021-02-25 17:55:50'),
(78, '2021-04-01', '2021-04-04', 'Carlos', '4', 'carlosge72@yahoo.com.ar', 'Hola tendras presupuesto para semana santa ?', '2021-02-26 02:19:14'),
(79, '2021-02-26', '2021-02-28', 'Maximiliano', '4', 'msd1790@gmail.com', 'Buenos dias estamos interesados en el alojamiento estariamos llegando hoy a las 20 o 21 hs y nos retirariamos el domingo 21hs aproximado saludos gracias', '2021-02-26 14:14:23'),
(80, '', '', 'nora romano', '', 'nora.romano20@gmail.com', 'Por favor quería saber cuanto cobran la noche y si tienen disponibilidad.\nGracias!', '2021-02-26 17:33:55'),
(81, '2021-03-05', '2021-03-07', 'Araceli Gimenez', '3', 'araceli-gimenez95@hotmail.com', 'Hola que tal quisiera saber el precio por los siguientes días . Gracias ', '2021-02-28 14:19:28'),
(82, '2021-03-31', '2021-04-04', 'Nancy', '5', 'nancabral@hotmail.com', 'buenas tardes, les agradeceré me informen si tienen disponibilidad para las fechas indicadas, en un solo espacio o dos, para4 o 5 adultos y una menor de 3 años. Gracias!!!', '2021-02-28 16:12:49'),
(83, '2021-04-12', '2021-04-17', 'Rocio caraballo', '6', 'Roycris05@gmail.com', 'Quería un presupuesto para esa fecha y medios y formas de pago', '2021-03-01 21:43:00'),
(84, '2021-03-05', '2021-03-07', 'Silvana Vero', '3', 'silvanavero8@gmail.com', 'Hola buenas tardes quería saber el valor por día. Y como.es el. Pago. Gracias. ', '2021-03-02 19:34:10'),
(85, '2021-03-05', '2021-03-08', 'Nicole Rabines', '5', 'rabinesnicole@gmail.com', 'Hola! Que tal? Queria saber el precio por 3 noches ?', '2021-03-02 20:25:36'),
(86, '2021-04-01', '2021-04-04', 'Verónica', '3', 'vepe87@gmail.com', 'Hola! Quisiera saber el valor para semana alta del duplex en planta alta. Gracias!', '2021-03-03 01:00:03'),
(87, '', '', 'Mabel Conde ', '', 'lalamabe@hotmail.com', 'Sería la fecha para abril .7 días 6 personas adultas.con dos perros.Necesitaria respuesta .precio.y si aceptan los.perros .muchas gracias', '2021-03-03 22:10:45'),
(88, '2021-03-19', '2021-03-21', 'Natalia', '4', 'albarracin.natalia83@gmail.com', 'Queria saber que precio estan las cabañas para 4 personas', '2021-03-04 17:06:43'),
(89, '2021-03-12', '2021-03-14', 'Micaela', '16', 'micaelacamilociro@gmail.com', 'Hola buenos días. Quería saber si tenían disponible del viernes 12/03 con salida el domingo 14.\nSomo 11 adultos y 5 menores. 1 de 13 años. 3 de 8 años y 1 de 2 años.\nGracias ', '2021-03-05 00:29:08'),
(90, '2021-04-02', '2021-04-04', 'Micaela', '7', 'mozzonimicaela@gmail.com', 'Buenas noches.\nEstoy interesada en conocer valores y disponibilidad para 7 adultas. Llegaríamos el viernes 2/4 y nos iríamos el domingo 4 de abril.\nDesde ya, muchas gracias.', '2021-03-06 00:14:07'),
(91, '2021-04-01', '2021-04-04', 'María Belén colazo', '4', 'mariabelencolazo@gmail.com', 'Buenas tardes, me pasarian precio para 4 personas, en semana santa. Gracias ', '2021-03-07 18:47:41'),
(92, '2021-03-31', '2021-04-04', 'VANINA', '2', 'vanina.martin26@gmail.com', 'Buenos días, quería averiguar disponibilidad y precios del fin de semana de pascuas. Gracias\nPd: Aceptan mascotas?\n', '2021-03-09 11:34:14'),
(93, '2021-03-13', '2021-03-14', 'Walter Mercuri', '5', 'walterhmercuri@yahoo.com.ar', 'Queria saber si tienen disponibilidad para este fin de semana, ingresando el sabado y retirandonos el domingo, ( una noche). Somos un matrimonio y 3 niños', '2021-03-09 13:09:45'),
(94, '', '', 'liliana', '', 'lilianamorales_70@hotmail.com', 'hola!quisiera saber si tenes lugar para el finde semana desde el 1 Abril al 4 de Abril,para 5 adultos y 2 menores,precio?y ubicacion?\ngracias,espero su respuesta.', '2021-03-10 20:51:10'),
(95, '2021-04-01', '2021-04-04', 'Micaela', '2', 'micasosa.a7@hotmail.com', 'Hola. Quería saber el valor del dúplex para 2-3 personas para Semana Santa? Y si había disponibilidad?', '2021-03-10 21:40:00'),
(96, '2021-04-02', '2021-04-04', 'MAIRA BAZAN', '13', 'bazanmairaalejandra@gmail.com', 'Buenas noches, por favor quería saber si tiene disponible para las fechas indicadas, seríamos 9 adultos y 4 niños de 5, 8, y 10 años.\nCuánto es su costo?\nDesde ya muchas gracias.\nSaludos,\nMaira', '2021-03-10 23:52:20'),
(97, '2021-04-14', '2021-04-21', 'Natalia', '3', 'natapsicosoc@gmail.com', 'Necesito saber si tienen disponible esas fechas, cuál es el valor y si aceptan mascota.\n\nMuchas gracias ', '2021-03-11 19:21:13'),
(98, '2021-03-20', '2021-03-22', 'MARIO Rodríguez', '2', 'lavaderodriguez@uolsinectis.com.ar', '', '2021-03-13 14:06:52'),
(99, '2021-03-19', '2021-03-22', 'Maria Rosa Minafaro', '2', 'mrminafa@yahoo.com.ar', 'Desearia saber si tienen disponibilidad y el precio a pagar alli. Gracias', '2021-03-16 16:52:23'),
(100, '2021-04-01', '2021-04-04', 'susana noemi Yalet', '3', 'syalet@hotmail.com', 'un adulto y dos chicos, de 6 y 10 años, quisiera un departamento chico.-\ngracias', '2021-03-17 15:06:39'),
(101, '2021-04-01', '2021-04-04', 'Cintia Jimenez', '5', 'jimcintia@gmail.com', 'Buenas, quería consultar cuánto nos sale por día y si podemos entrar el jueves a la mañana y salir el domingo a la tarde/noche. \nQuedo a la espera de sus comentarios. \nSlds ', '2021-03-18 01:21:12'),
(102, '2021-04-03', '2021-04-04', 'Carlos balbuena', '5', 'balbuenacarlosraul@gmail.com', 'Hola quisiera saber  si tienen disponibilidad para ESA fechar y cuanto costaria lá estadia Muchas gracias', '2021-03-18 01:30:29'),
(103, '2021-03-21', '2021-03-26', 'BiancaMelisa', '2', 'Melherner@gmail.com', 'queria  saber cuanto me salen las 5 noches. gracias ', '2021-03-18 03:40:06'),
(104, '2021-04-01', '2021-04-04', 'Roberto', '9', 'rfk22@hotmail.com.ar', 'Hola me pasarían precio x favor para las fechas indicadas? Aceptan mascota? (Es una caniche muy tranquila)', '2021-03-22 13:59:36'),
(105, '2021-04-01', '2021-04-03', 'wanda', '4', 'wanda_ganino@hotmail.com', 'Estimados: Quisiera saber si tienen disponibilidad para la fecha indicada (2 noches) y el costo. Aguardo sus comentarios. Gracias! Saludos', '2021-03-23 20:54:19'),
(106, '2021-04-01', '2021-04-04', 'Ariel Landriel', '2', 'ariel.landriel23@gmail.com', 'Buenas!! quiero saber si hay disponibilidad para la fecha mencionada, y la tarifa. Desde ya muchas gracias, saludos!', '2021-03-27 19:05:49'),
(107, '2021-04-02', '2021-04-05', 'Micaela Ponce', '4', 'micaelaponce1@hotmail.com', 'Queria consultar disponibilidad y precios para 2 adultos y 2 niños de 9 y 6 años. Muchas gracias! ', '2021-03-29 17:31:22'),
(108, '2021-04-02', '2021-04-04', 'Maria', '4', 'sebahoyos1704@gmail.com', 'Somos cuatro en total matrimonio y dos niños por favor me pasarías precio', '2021-03-29 21:19:35'),
(109, '2021-04-16', '2021-04-18', 'María Luisa', '2', 'luisalascano@hotmail.com', 'Quisiera saber disponibilidad y tarifa para dos personas. Muchas gracias', '2021-04-06 01:10:32'),
(110, '2021-04-19', '2021-04-25', 'Veronica Ditzel', '3', 'veyodi@gmail.com', 'Quiero saber el valo por una semana . gracias', '2021-04-12 18:52:51'),
(111, '', '', 'analia valerio', '2', 'analiabvalerio@hotmail.com', 'queria saber sobre cabaña del complejo las olas para dos personas...fin de semana de mayo...cualquiera de ellos...gracias.', '2021-04-12 20:35:25'),
(112, '2021-04-18', '2021-04-20', 'martin casado', '2', 'tinchocasado@hotmail.com', 'Disponibilidad y tarifas', '2021-04-16 15:51:18'),
(113, '', '', 'Laura Di Santo', '', 'laurayesica.disanto4@gmail.com', 'Buenas!! Quería saber precio por dos dias y una noche para una pareja sólos, sería sábado y domingo, gracias ', '2021-04-18 22:47:16'),
(114, '2021-08-25', '2021-08-31', 'Emiliano', '3', 'emi_publicas@hotmail.com', 'Hola, quiero pasar unos días con mi esposa y me bebé para esa fecha, me podrían pasar info, gracias', '2021-04-24 18:01:15'),
(115, '2022-01-17', '2022-01-24', 'Yanina', '7', 'yanijuarez@hotmail.com', 'Quería saber cuánto me sale 6 adultos y un menos de 4 años para la fecha que seleccioné. Y como es para abonar y demás. Muchas gracias', '2021-09-05 22:18:42'),
(116, '2021-09-18', '2021-09-19', 'Graciela', '4', 'Gracielalela2009@hotmail.com', 'Precio x día x persona .somos 4 personas adultas flia ', '2021-09-06 03:30:57'),
(117, '2021-09-10', '2021-09-12', 'veronica', '2', 'vbrianti@yahoo.com.ar', 'somo 2 adultso y un niño de 4 años', '2021-09-07 14:30:36'),
(118, '2021-10-08', '2021-10-10', 'Brian Diaz ', '3', 'briandiazabel@hotmail.com', 'Hola que tal, quería saber el precio de las cabañas, para un finde semana, somos 2 mayores y un menos de 2 años. Gracias. ', '2021-09-08 00:43:58'),
(119, '2022-01-01', '2022-01-10', 'veronica', '4', 'veronicalcasado@gmail.com', 'Buenas noches somos grupo familiar de 4 personas y la idea es viajar del 1 de enero hasta el 10 de enero. Podrias decirme cuanto nos saldria? Muchas gracias', '2021-09-09 00:41:01'),
(120, '2022-01-03', '2022-01-10', 'Gustavo daniel Ramirez', '3', 'gus10ramirez@hotmail.com', 'Hola q tal queria saber disponibilidad para las fechas ingresadas y si podian mandarme precios ', '2021-09-09 04:27:38'),
(121, '2022-01-09', '2022-01-23', 'Ana Carolina López', '-5', 'anacarolinalopez2011@hotmail.com', 'Hola buenos días! Quisiera saber precio y disponibilidad para las fechas solicitadas. Las mismas no son definitivas todavía tengo disponibilidad. Muchas gracias!', '2021-09-09 20:07:47'),
(122, '2021-09-17', '2021-09-19', 'silvia del rosso', '5', 'silviadelrosso@yahoo.com.ar', 'tarifas. gracias', '2021-09-09 23:57:43'),
(123, '2022-01-23', '2022-01-30', 'Mario Cesar Flores', '6', 'floresgarcia1963@hotmail.com', 'quisiera mas información como costo formas de pago disponibilidad etc.', '2021-09-10 13:12:44'),
(124, '', '', 'Susana', '', 'sbbaldo@yahoo.com.ar', 'Buen día. Desearía mis conocer precio, y posibilidades de alquiler para las fechas del 02 al 14 de enero de 2022. Gracias. Somos dos adultos y una niña de 6 años. ', '2021-09-14 14:42:05'),
(125, '', '', 'Omar Pittelli', '6', 'opittelli@gmail.com', 'Buenas noches, estoy buscando una casa para 6 personas una semana ,puede ser enero o febrero.Gracias', '2021-09-16 23:41:10'),
(126, '2021-12-26', '2022-01-03', 'Noemi ', '4', 'nac.1977@hotmail.com', 'Hola : Quería saber si tenes el precio desde el 26/12 /21 al 03/01/22.\nPara pareja con mellizos de 6 años.\nSi me podrían decir el precio.\nGracias.', '2021-09-17 19:48:15'),
(127, '', '', 'Andrea', '', 'fre.andrea@yahoo.com.ar', 'Hola disponibilidad para el finde de octubre del 8 al 11 familia con 2 menores gracias', '2021-09-19 15:58:48'),
(128, '', '', 'Carina Arnoldi', '8', 'psparnoldi@hotmail.com', 'Deseo consultar disponibilidad y precios para la segunda quincena de enero o primera semana de febrero. Somos dos familias )matrimonio con 2 adolescentes). Deseamos estar una semana. \n Además si aceptan mascotas y están adheridos a PREVIAJE. Muchas gracias!', '2021-09-22 01:16:38'),
(129, '2021-10-18', '2021-10-23', 'Daniel Carmona', '8', 'argentinoporteria@gmail.com', 'Hola buenos dias, mi nombre es daniel y queria consultar por alojamiento para mi y mi familia, seriamos 8 personas, 6 hijos de 2, 5, 11,13,15 y 17 años mas mi señora y yo.\nqueria conocer disponibilidad, valores y servicios incluidos.\nDesde ya,  muchas gracias', '2021-09-23 13:40:47'),
(130, '2022-02-12', '2022-02-18', 'celina molina carranza', '3', 'celinmolin@gmail.com', 'Buenas tardes, me contacto para pedir informacion de costos para el mes de Febrero de 2022. Somos 2 adultos y un niño de 14\nQuedo expectante a su respusta\nMuchas gracias', '2021-09-23 19:59:52'),
(131, '2022-02-05', '2022-02-12', 'jorge', '-2', 'dj_bebus@yahoo.com.ar', 'hola bd aproximadamente cuanto me saldria x 7 dia somos 2 mi sra y yo', '2021-09-24 14:41:42'),
(132, '2021-10-07', '2021-10-24', 'Mariana', '5', 'marianasabinpaz@yahoo.com.ar', 'Hola quisiera saber precios del 7/10 al 11/10 y del 8/10 al 11/10, somos un matrimonio y 3 niños, gracias', '2021-09-24 17:41:15'),
(133, '', '', 'Andrea Zanchetta', '', 'andrea.zanchetta22@hotmail.com', 'Buenas noches . Queria saber cuanto.costaria la segunda quincena de diciembre y la.primera de enero (todavía no tenemos seguro q quincena decidiremos.) En departamento para 4 personas. ? Y saber si aceptan mascota. ?(Perro golden)', '2021-09-25 01:11:21'),
(134, '2022-01-14', '2022-01-21', 'Sebastian', '5', 'sebacerr@hotmail.com.ar', 'Buenas ,somos 2 adultos ,3 menores \nEnviar información gracias', '2021-09-26 15:34:59'),
(135, '2021-10-15', '2021-10-16', 'Marians', '5', 'mariana_chasco@yahoo.com ar', 'Hola! Quisiera saber el costo x noche de un duplex para 5 personas para la fecha del 14/10 al 16/10.\nGracias!', '2021-09-26 18:00:00'),
(136, '2021-12-29', '2022-01-04', 'María Silvina Rey', '4', 'mariasilvinareyat@gmail.com', 'Hola quisiera saber precio y disponibilidad para alojarnos con mi marido y mis dos hijos de 13 y 16 años desde el 29 hasta el 4 de enero y si tienen pre-viaje.Saludos .Silvina ', '2021-09-27 20:11:23'),
(137, '2021-09-23', '2021-10-26', 'Mirtha', '3', 'mirthaestevez1@hotmail.com', 'Detalles del lugar y precio', '2021-09-27 23:13:57'),
(138, '2021-10-22', '2021-10-26', 'Mirtha', '3', 'mirthaestevez1@hotmail.com', 'Detalles y precio\n', '2021-09-27 23:16:53'),
(139, '2021-10-08', '2021-10-11', 'Ezequiel matias vignolo hilu', '4', 'ezequielvignolo@hotmail.com', 'Buen día. quería saber el precio por 3 noches del fin de semana largo de Octubre. Gracias\n\nSaludos\nEzequiel Vignolo\n', '2021-09-28 13:36:30'),
(140, '2021-10-07', '2021-10-11', 'Marcela ', '1', 'Maralejdg@hotmail.com', 'Buenas! Quisiera ir el feriado de octubre desde el jueves,llegaría  a la tarde hasta el lunes...\nAceptan perro?\nPrecio?\nGracias!!!', '2021-09-28 21:22:39'),
(141, '2021-10-08', '2021-10-11', 'Sofia', '', 'Sofy3382@hotmail.com', 'Hola queria saber si tenian disponible la de 60 mts y su precio\nGracias', '2021-09-28 21:27:28'),
(142, '2021-10-08', '2021-10-11', 'PATRICIA  FERRARI', '-6', 'patricia.ferrari@braycom.com', 'Hola buenas tardes, somos 6 adultos con un menor de 3 años. Quisiera saber si tienen lugar en esa fecha y cual seria el costo\nAguardo la respuesta, gracias', '2021-09-29 17:14:52'),
(143, '2021-10-09', '2021-10-11', 'ezequiel brunner', '4', 'ezebrunner@gmail.com', 'hola, somos dos adultos y dos niños. cuanto me saldría dos noches?', '2021-09-29 19:17:47'),
(144, '2022-01-15', '2022-01-31', 'Claudio', '-4', 'cnavazzotti@hotmail.com.ar', 'Hola, quisiera saber el precio. Si tiene parrilla y que se comparte. Gracias.- ', '2021-09-30 14:46:37'),
(145, '2021-10-08', '2021-10-11', 'Micaela Vanesa imoli', '5', 'micaelaimoli@gmail.com', 'Somos 4 adultos y un niño de 3 años quiero saber precio y disponibilidad', '2021-10-01 13:21:55'),
(146, '2022-01-15', '2022-01-22', 'Antonela', '2', 'antofrontinis@gmail.com', 'hola buenos dias. queria averiguar el precio de la estadia desde el 15 de enero al 22 de enero. somos 1 mayor y un nene de 10 años. gracias', '2021-10-01 16:09:30'),
(147, '2022-02-12', '2022-02-27', 'Oscar Jesús', '2', 'oscar.jesuss@yahoo.com', 'cuanto sería el alquiler para la temporada 2022. la segunda quincena de febrero pero arrancando desde el día 12 hasta el 27. le agradezco por su respuesta atte. oscar ', '2021-10-03 23:29:35'),
(148, '2022-02-12', '2022-02-27', 'Oscar Jesús', '2', 'oscar.jesuss@yahoo.com', 'cuanto sería el alquiler para la temporada 2022. la segunda quincena de febrero pero arrancando desde el día 12 hasta el 27. le agradezco por su respuesta atte. oscar ', '2021-10-03 23:29:35'),
(149, '2021-10-09', '2021-10-12', 'MARCELA GODOY', '3', 'rh.marcelagodoy@gmail.com', 'Hola !! Quisiera saber si hay disponibilidad y costos. \nGracias!!', '2021-10-04 19:19:27'),
(150, '2021-10-06', '2021-10-11', 'Susana Facendini', '', 'susanafacendini@yahoo.com.ar', 'Quisierasaber si tenen alojamiento para una familia (matrimonio   1 hijo de 19   1 hija de 9)\natte\nsusana', '2021-10-04 20:36:47'),
(151, '2022-01-15', '2022-01-22', 'betiana', '2', 'betiana123rios@hotmail.com', 'hola quería saber bien los metros que hay desde el mar al complejo.y precio x noche o que me expliques como se realizan las reservas desde ya muchas gravias', '2021-10-05 21:27:26'),
(152, '2021-10-09', '2021-10-11', 'Fabiana Mabel Correa', '3', 'fabianacorrealopez@hotmail.com', 'Buenas Noches. Quiero saber si tenés algo para esos días. Precio por favor? Está en el mismo San Clemente del Tuyu', '2021-10-06 01:41:53'),
(153, '2021-10-09', '2021-10-11', 'Fabiana Mabel Correa', '3', 'fabianacorrealopez@hotmail.com', 'Buenas Noches. Quiero saber si tenés algo para esos días. Precio por favor? Está en el mismo San Clemente del Tuyu', '2021-10-06 01:41:54'),
(154, '2021-10-09', '2021-10-11', 'Fabiana Mabel Correa', '3', 'fabianacorrealopez@hotmail.com', 'Buenas Noches. Quiero saber si tenés algo para esos días. Precio por favor? Está en el mismo San Clemente del Tuyu', '2021-10-06 01:41:54'),
(155, '2021-10-09', '2021-10-11', 'Fabiana Mabel Correa', '3', 'fabianacorrealopez@hotmail.com', 'Buenas Noches. Quiero saber si tenés algo para esos días. Precio por favor? Está en el mismo San Clemente del Tuyu', '2021-10-06 01:41:55'),
(156, '2022-01-03', '2022-01-18', 'Andres', '4', 'andres1976@hotmail.com', 'Queria saber que nos pueden ofrecer para esa fecha. Somos una familia de 4 personas 2 adultos y 2 menores. ( 4 y 7 años)\nMuchas gracias', '2021-10-06 12:29:04'),
(157, '2022-01-17', '2022-01-24', 'julian', '4', 'el_vasco82@hotmail.com', 'hola estamos interesados en ir en la fecha indicada', '2021-10-07 00:48:00'),
(158, '', '', 'Karina', '2', 'rikana@gmail.com', 'Buenas tardes, aceptan mascota?', '2021-10-07 17:48:32'),
(159, '2022-02-05', '2022-02-12', 'Miguel', '8', 'miguelahernandez1204@gmail.com', 'Factibilidad y costo por tres apartamentos.\n1) para un matrimonio y dos hijos menores.\n2) para un matrimonio y dos hijos menores.\n3) para dos matrimonios y un hijo menor y un bebe.\nSon tres los departamentos para la fecha indicada.\n', '2021-10-08 16:06:19'),
(160, '2022-02-12', '2022-02-19', 'Fernanda', '4', 'ferchi@gmail.com', 'hola queria cosnutlar por precios para una semana en febrero', '2021-10-08 21:42:42'),
(161, '2022-02-12', '2022-03-03', 'Dalila Pozzi ', '3', 'pozzidalila@gmail.com', 'Hola!,\nNos gustaría que nos informen costo y disponibilidad para realizar una reserva en un departamento para una pareja con un bebé de 18 meses, desde el día 12/02 hasta el día 03/03/2022.\nGracias, saludos\nDalila', '2021-10-08 23:46:51'),
(162, '2022-01-02', '2022-01-09', 'Veronica', '3', 'veronicacas1980@gmail.com', 'Hola,me podrás pasar precio?', '2021-10-10 18:11:10'),
(163, '2021-12-31', '2022-01-08', 'Malena', '3', 'malena_puente@hotmail.com', 'Hola. Buenas tardes. Quería saber precios para la semana arriba ingresada.\nGracias', '2021-10-11 17:32:54'),
(164, '2022-02-04', '2022-02-11', 'Luciana', '5', 'busquedaslm@gmail.com', 'Queria saber el costo para una semana en febrero ( pteferentemente del 4 al 11) si tienen disponibilidad y previaje. Gracias', '2021-10-12 01:50:55'),
(165, '', '', 'Debora Farias', '', 'debiifarias06@gmail.com', 'Hola, con mi familia (4) estamos buscando para pasar 2 días y una noche. Quería saber ¿cuál sería el precio? Y si ¿Se cobra por noche o día?', '2021-10-12 06:11:23'),
(166, '2022-01-23', '2022-01-30', 'SOLEDAD', '2', 'soledadrligo@hotmail.com', 'quisiera conocer tarifa de alojamiento para 2 y otro para una pareja con un nene de 6  años y 2 bebes ', '2021-10-12 14:44:32'),
(167, '2022-02-14', '2022-02-20', 'Héctor', '4', 'clarisaandrade84@gmail.com', 'Quería saber cuánto me saldría para esa fecha el alquiler ', '2021-10-13 00:54:10'),
(168, '2022-02-01', '2022-02-08', 'Antonela', '4', 'anto.carbonelli@hotmail.com', 'Hola! Quisiera saber disponibilidad para la fecha ingresada y tarifas, gracias!', '2021-10-13 04:48:29'),
(169, '2021-12-23', '2022-01-02', 'Julieta Osorez', '2', 'josorez@gmail.com', 'Hola somos mi hija y yo.  Cuanto seria el precio?', '2021-10-13 16:44:49'),
(170, '2022-02-07', '2022-02-14', 'Maria', '3', 'flornico70@gmail.com', 'Precio y disp para 3 personas.estan adheridos a previaje?', '2021-10-14 02:22:38'),
(171, '2022-01-12', '2022-01-16', 'Sosa Gerardo', '2', 'sosagerardo.m@gmail.com', 'Hola, mí consulta es si se puede reservar para esa fecha puesta un dpto, y como serian los pasos a seguir? Gracias', '2021-10-15 03:12:12'),
(172, '2021-10-23', '2021-10-24', 'Paola Manfredo', '3', 'paolamanfredo2014@yahoo.com.ar', 'Buenas tardes  quisiera saber si tienen alojamiento para 2 adultos y 1 menor para esa fecha. Precio por día. Gracias', '2021-10-15 15:15:43'),
(173, '', '', 'Erica Coronel', '', 'ericadcoronel@gmail.com', 'Quisiera saber el valor de la cabaña para tres personas para febrero.\n\nGracias', '2021-10-16 01:59:17'),
(174, '2022-02-06', '2022-02-12', 'Walterdessana', '4', 'Walterdessana@hotmail.com', '', '2021-10-17 02:52:56'),
(175, '2021-10-23', '2021-10-24', 'Sebastian', '4', 'sar80_so@hotmail.com', 'Hola,quisiera saber precios por una estadía 2 mayores 2 menores de 13 años', '2021-10-17 21:11:45'),
(176, '2022-01-08', '2022-01-15', 'Sabrina', '4', 'donnarummasabrina6@gmail.com', 'Hola, quería consultar precios para una semana y 4 personas. Gracias! ', '2021-10-17 23:38:52'),
(177, '2021-12-28', '2022-01-05', 'Karina', '4', 'karu_926@hotmail.com', 'Somos una pareja con 2 niños de 8 y 6 años, y un perro , aceptan?\nQuisieramos saber el valor.\nGracias', '2021-10-19 01:33:03'),
(178, '', '', 'Silvia Susana Canade', '', 'silviasucanade54@gmail.com', 'Hola ! Solicito precio para 3 adultos del 17/1/2022 al 24/1/2022.\nTengo 1 mascota chiquita.\nGracias.', '2021-10-19 05:53:32'),
(179, '2022-01-03', '2022-01-13', 'Geraldin Torelli', '4', 'morabialabia@hotmail.com', 'hola, necesito info para depto 4 adultos y un bebe. acepatan mascota?\nseria en enero del 3 al 13. gracias', '2021-10-21 19:18:54'),
(180, '2021-11-11', '2021-11-13', 'Rosana', '4', 'rosanapierro@yahoo.com.ar', 'Queriamos saber costo de alojamiento para un matrimonio con 2 niñas para imgresar 11 de noviembre y salir 13 de noviembre. Gracias', '2021-10-21 23:57:48'),
(181, '2022-01-18', '2022-01-24', 'Daniela Olivera', '8', 'danielaoviedo879@gmail.com', 'Hola buen día quería consultar para el 18 al 24 enero para 8 personas. Disponible y precio espero su respuesta. Gracias', '2021-10-24 16:44:15'),
(182, '2021-12-31', '2022-01-03', 'Marcela', '5', 'prof.marcelagarcia@gmail.com', 'Quisiera saber si cuentan con disponibilidad y la tarifa ', '2021-10-24 19:15:55'),
(183, '2021-12-23', '2021-12-26', 'marcelo', '4', 'cnacad@gmail.com', 'Hola, quisiera consultar disponibilidad y precio para un duplex para 4 personas, por 3 noches desde el 23/12 hasta el 26/12. Aguardo su respuesta. Muchas gracias! ', '2021-10-24 21:24:19'),
(184, '2021-12-16', '2021-12-23', 'Mariela', '6', 'mgilfeilberg@gmail.com', 'Hola. Somos dos matrimonios y dos nenas.\nAdmiten mascotas? Gracias', '2021-10-25 00:17:10'),
(185, '2022-02-14', '2022-02-27', 'Patricia', '2', 'patriciacami_84@hotmail.com', 'Buenos días\n\nMe gustaría que me enviaran si Un tan amables las tarifas para la estadía estimada para mí  pequeña de 5 y yo. Cuánto sería por una semana y cuánto por los 14 días.\n\nQuedo a la espera de una pronta respuesta\n\nSaludos cordiales', '2021-10-25 02:51:34'),
(186, '2022-01-23', '2022-01-30', 'Gustavo Guillermo Vasquez', '4', 'ggvasquez04@gmail.com', 'Hola para el mes de enero en la fecha indicada tienen disponibilidad?\nAceptan mascotas?\nMil gracias espero tu respuesta.\nGustavo', '2021-10-25 07:42:21'),
(187, '2021-12-29', '2022-01-01', 'Claudio González', '2', 'klaudio_666_94@hotmail.com', 'Hola que tal, me llamo Claudio, quería saber si tiene disponibilidad para esas fechas, quisiera saber tmb los precios, desde ya muchísimas gracias', '2021-10-25 13:51:28'),
(188, '2022-01-31', '2022-02-07', 'natalia', '5', 'natalia.funes.arq@gmail.com', 'hola buenos dias... queria saber si tenias disponibilidad y los costos para la fecha indicada, para 5 personas, con 2 habitaciones.\nEspero tus comentarios', '2021-10-25 15:29:06'),
(189, '2022-01-03', '2022-01-18', 'Jose luis', '6', 'dr.joseavellaneda@gmail.com', 'Somos 2 adultos y 4 niños.Estas incluido en previaje?', '2021-10-26 01:03:24'),
(190, '2021-11-20', '2021-11-22', 'Victor', '8', 'gringovictor18@gmail.com', 'Quisiera saber por una cabaña para una o dos noches para aprovechar para ir a mundo marino y las termas', '2021-10-26 23:46:26'),
(191, '2021-12-22', '2021-12-30', 'Maria Belen Hermann', '3', 'hnn_mariabelen@outlook.com', 'hola, cuanto sale el hospedaje para señora sola con dos niños, por favor?', '2021-10-28 01:00:32'),
(192, '2022-01-02', '2022-01-09', 'Nestor Yerio', '3', 'nestoryerio@gmail.com', 'Necesito saber disponibilidad y condiciones para poder realizar la reserva. Muchas gracias!', '2021-10-28 23:17:29'),
(193, '2021-12-30', '2022-01-02', 'Maria', '2', 'marygeandet@hormail.com', 'Hola quería saber disponibilidad y tarifa para dos adultos y si aceptan mascota.', '2021-10-29 00:42:58'),
(194, '2022-02-13', '2022-02-18', 'Milasni Castillo', '4', 'milasni1980@gmail.com', 'Cuánto me saldría para una semana somos 4 personas en la semana de febrero', '2021-10-30 00:45:15'),
(195, '2022-01-03', '2022-01-17', 'Griselda', '4', 'griseldapuchetagp@gmail.com', 'Quisiera saber si tenes disponible para esa fecha y precio', '2021-10-30 10:12:13'),
(196, '2022-01-18', '2022-01-21', 'Mara Damonte', '3', 'maradamonte@live.com.ar', 'Quería saber precio y disponibilidad! Gracias!!!!', '2021-10-30 23:48:29'),
(197, '2021-11-13', '2021-11-14', 'Patricia Gonzalez', '4', 'patito452@hotmail.com', 'hola quisiera saber precio para 4 dos niños y dos adultos para la fecha mencionada con baño privado, muchas gracias', '2021-10-31 20:33:28'),
(198, '2022-01-15', '2022-01-24', 'Ana Julia Cuadrado', '4', 'cuadradoanajulia@gmail.com', 'buen dia quisiera saber la disponibilidad y precio para la fecha consultada somos un matrimonio y dos chicos de 6 y 10 años- gracias', '2021-11-02 13:51:07'),
(199, '2021-11-20', '2021-11-22', 'Sandra Fabiana', '4', 'grecosandra@hotmail.com', 'Buenos dias es para el feriado de noviembre  somos 4 y un menos de 1 año y medio me pasas precio y si esta cerca de mundo marino', '2021-11-04 15:49:09'),
(200, '2022-02-13', '2022-02-19', 'Ariel Colman', '3', 'arielcolman118@gmail.com', 'Buen día, por favor informar precio de estadía, si no cuentan con disponibilidad en las fechas indicadas brindar alternativas.\nMuchas gracias', '2021-11-05 12:51:18'),
(201, '2021-11-20', '2021-11-22', 'Aldana', '4', 'aldanapf@hotmail.com', 'Hola,queria saber si tenian disponibilidad. Somos dos adultos y dos niños de 10 y 12 años. Gracias', '2021-11-06 01:02:24'),
(202, '2021-11-27', '2022-01-03', 'juan', '4', 'juaneduardochocobar@yahoo.com', 'somos un matrimonio y dos chicos, quiero saber sobre el precio por favor', '2021-11-06 01:29:43'),
(203, '2021-11-29', '2021-12-03', 'Melina', '3', 'melinantonella98@gmail.com', 'Presupuesto y que incluye. ', '2021-11-06 13:43:02'),
(204, '2022-02-26', '2022-03-01', 'Cristian', '5', 'crisjvazquez@gmail.com', 'Hola buenas tardes, estamos buscando alojamiento para pasar unos días de vacaciones. \nVi que el complejo tiene pileta y plaza de juego para nuestras niñas. ¿Ustedes me podrán pasar un presupuesto? \nCheck in: 26/2 Check out: 1/3 Somos una familia de 2 adultos y 3 menores (edades de 9, 8 y 2). \nTambién me gustaría saber si existe la posibilidad de ingresar el viernes 25 por la noche (cerca de las 22h). \nAguardo vuestros comentarios. \nMuchas gracias \nCristian Vazquez', '2021-11-06 16:44:59'),
(205, '2021-12-25', '2021-12-30', 'Irina Torres', '3', 'irina.torres69@gmail.com', 'Hola, quisiera un alojamiento con dos habitaciones, una para pareja y una para adulto sólo. Necesito saber si aceptan mascotas (4 chicas) y cuanto costaría el alojamiento.', '2021-11-07 16:21:16'),
(206, '2022-01-10', '2022-01-14', 'Alejandro Crespo', '4', 'ale.gabriel.crespo@gmail.com', 'Buenas tardes, estoy buscando alojamiento para 4 personas.\nGracias', '2021-11-07 21:57:55'),
(207, '2022-01-23', '2022-01-31', 'Silvia Susana Canade', '3', 'silviasucanade54@gmail.com', 'Hola ! Quisiera saber precio por noche para 3 adultos.\nSí aceptan mascota chiquita.\nY si poseen pileta. Gracias.', '2021-11-08 06:22:48'),
(208, '2022-01-23', '2022-01-31', 'Silvia Susana Canade', '3', 'silviasucanade54@gmail.com', '', '2021-11-08 06:23:50'),
(209, '2022-01-24', '2022-01-31', 'Gisela Aranda', '3', 'aranda.gisel@gmail.com', 'Hola somos dos adultos y un menor de 7 años. Buscamos lugar para la segunda quincena de enero.', '2021-11-08 15:07:20'),
(210, '2022-01-03', '2022-01-07', 'Karina', '2', 'karinapaolakloster@gmail.com', 'Hola buenas tardes, me podrian decir el precio para dos personas? Y con cuanto tiempo hay que reservar? Graciass!', '2021-11-08 17:37:11'),
(211, '2021-11-15', '2021-11-21', 'María Victoria Tutino', '3', 'tutino_maia@yahoo.com.ar', 'Buen día, quería conocer disponibilidad, tarifas y servicios para las fechas indicadas. Somos un matrimonio con un nene de 5 años.\nTambién si cuentan con cochera.\nGracias', '2021-11-10 13:56:36'),
(212, '2021-12-30', '2022-01-16', 'Lorena', '4', 'lorenagiussani1977@gmail.com', 'Hola, quería saber si tienen disponibilidad y cuanto es el valor por la estadia', '2021-11-10 16:54:17'),
(213, '', '', 'Daniela ', '', 'danielaloza79@gmail.com', 'Quisiera saber qué disponibilidad tienen entre el 26 y el 30 de diciembre, y cuáles son los precios. ', '2021-11-13 15:17:47'),
(214, '2022-01-08', '2022-01-22', 'Susana Poletto', '4', 'susanapoletto@gmail.com', 'Buenos dias quisiera consultar disponibilidad y precio en su complejo La Lucia, somos un familia de 2 adultos y dos menores.\ndesde ya muchas gracias.\nSusana ', '2021-11-13 16:00:50'),
(215, '2022-01-17', '2022-01-31', 'Mario', '3', 'marioromeroscasso@hotmail.com', 'Sala comedor cocina, dormitorio principal y baño. Precio ', '2021-11-13 22:35:43'),
(216, '2021-11-19', '2021-11-22', 'Germán', '4', 'barriosgerman86@gmail.com', 'Buen día, disponibilidad desde el 19/11 hasta el 22/11. 2 adultos, 1adolescente y un niño de 3años.', '2021-11-14 12:48:16'),
(217, '2022-01-29', '2022-02-05', 'Carolina', '4', 'caro.cattani2014@gmail.com', 'Tambien puede ser para la ultima semana de enero', '2021-11-14 13:47:59'),
(218, '2021-12-13', '2021-12-17', 'eduardo vicente toledo', '2', 'darioeduardovicentetoledo@gmail.com', 'precio por dia para 2 personas de 66años planta vaja ,cochera y wifi y si se puede pescar de costa frente a los departamentos', '2021-11-14 17:22:40'),
(219, '2022-01-03', '2022-01-10', 'Marina', '3', 'marinamiloc@gmail.com', 'Queremos hacer reserva para esta fecha. Tenés disponible.\nPrecio?', '2021-11-14 18:44:35'),
(220, '2021-12-31', '2022-01-07', 'damian', '3', 'fortinero_vasco@hotmail.com', 'hola, queria saber para dos adultos y un menor de 12 tarifa y disponibilidad del 31 de diciembre 7 noches gracias', '2021-11-15 03:04:52'),
(221, '2021-11-20', '2021-11-21', 'Mónica Beatriz', '2', 'Mbdo0611@gmail,com', 'Cuanto cuesta x noche?', '2021-11-15 04:31:03'),
(222, '2022-01-03', '2022-01-17', 'EDGAR GUARDATTI', '-2', 'eguardatti@montich.com.ar', 'precio x quincena ', '2021-11-15 15:45:56'),
(223, '2022-01-15', '2022-01-24', 'Ana Julia Cuadrado', '4', 'cuadradoanajulia@gmail.com', 'buen dia quisiera consultar sobre disponibilidad para la fecha indicada somos un matrimonio y dos chicos y cual seria el valor. gracias', '2021-11-16 11:47:30'),
(224, '2022-02-19', '2022-02-26', 'CAROLINA LUCO', '3', 'profesoracarolinaluco@gmail.com', 'BUENOS DIAS ESTOY BUSCANDO CABAÑAS O HOSPEDAJES PARA UN MATRIMONIO CON UN NIÑO DE 5 AÑOS AGUARDO UNFORMACION GRACIAS', '2021-11-16 14:11:07'),
(225, '2021-11-20', '2021-11-22', 'Carina ', '4', 'caririquel80@gmail.com', 'Tienen disponibilidad y somos un matrimonio ,un bebé y una nena de 12 años', '2021-11-17 17:52:34'),
(226, '2022-01-03', '2022-01-10', 'Ivanna', '8', 'delatorreivanna35@gmail.com', 'Somos 4 adultos y 4 niños', '2021-11-17 18:11:21'),
(227, '2022-02-07', '2022-02-14', 'Carlos', '2', 'carjafranco@gmail.com', 'Somos mi señora y yo cuánto salen los 7 días con garaje?', '2021-11-19 01:39:12'),
(228, '2022-01-17', '2022-01-27', 'Joana Antonela Schreiner', '3', 'joaa.as@hotmail.com', 'Somos 2 adultos y una niña (5años) queriamos  precio para una cabaña', '2021-11-19 09:04:51'),
(229, '2021-11-19', '2021-11-22', 'José Carrasco', '4', 'josecarrascolopez4@gmail.com', 'Somos dos adultos un bebé de 9 meses y un niño de 9 años que precio económico tendrías por noche', '2021-11-19 15:55:59'),
(230, '2022-01-03', '2022-01-14', 'Jorge Sanchez', '5', 'Jorgeas2ar@gmail.com', 'Muy buen dia. Les consuto somos matrimonio con tres chicos ( 20-17-12 años), somos de Mendoza, tienen diposnibilidad desde el 03 al 14 inclusive.\nSi es favorable les pido forma de seña y costo. Gracias', '2021-11-20 11:21:39'),
(231, '2021-12-16', '2021-12-23', 'paula', '1', 'pauladelbarba@hotmail.com', 'con una mascota', '2021-11-21 14:52:18'),
(232, '2022-02-23', '2022-02-28', 'Gabriela', '3', 'gabrielacarfora@gmail.com', 'Quería saber el costo de esos dias', '2021-11-21 23:58:48'),
(233, '2022-02-06', '2022-02-12', 'Fernando', '2', 'lpmantenimientos@gmail.com', 'Estimados,tienen disponibilidad en esta fecha y que costo tiene.\nFernando', '2021-11-22 11:57:10'),
(234, '2022-01-03', '2022-01-14', 'Jorge Sanchez', '5', 'jorgeas2ar@gmail.com', '¡ Muy buen día! Somos familia de Mendoza ( Matrimonio   3 chicos), les cpnsulto disponibilidad desde el 03 al 14/01/22. Su costo y forma de reserva. Gracias', '2021-11-22 12:41:18'),
(235, '2022-01-10', '2022-01-13', 'Lorena', '4', 'mlorenac12@gmail.com', 'Quería disponibilidad y precio para 4 días. Del 10 al 13. 2 adultos y 2 niños ( 9 y 2 años)', '2021-11-23 18:04:44'),
(236, '2021-12-31', '2022-01-02', 'Evelyn Ayelen Mora', '2', 'evyayemor@gmail.com', 'Buenas tardes. Quería saber si están alquilando para ese fecha. Somos dos adultos. ', '2021-11-23 20:43:26'),
(237, '2021-11-26', '2021-11-29', 'Diego Markow', '2', 'markowdiego3@gmail.com', 'Hola.queria saber prexios y como reservo', '2021-11-23 23:41:05'),
(238, '2022-01-17', '2022-01-24', 'Marta', '3', 'susygimenez147@gmail.com', 'Tarifa por favor .aceptan mascota pequeña', '2021-11-25 18:16:19'),
(239, '2021-12-03', '2021-12-05', 'Paula Mariana PEREZ', '3', 'pmpmariana@gmail.com', 'buenas tardes quería consultar disponibilidad y valores para 3 adultos y un nene de 2 años y medio.\nllegaríamos el viernes 3 y nos vamos el domingo 5.\ndesde ya muchas gracias\nsaludos\npaula', '2021-11-25 20:52:18'),
(240, '2022-01-30', '2022-02-09', 'Rosana', '4', 'rosanarussillo@yahoo.com.ar', 'Hola.. busco alojamiento para 4 personas del 29 o 30 de enero al 8 o 9 de febrero.\nGracias', '2021-11-26 00:14:50');
INSERT INTO `contacto` (`id`, `fecha_desde`, `fecha_hasta`, `nombre`, `cantidad`, `email`, `consulta`, `fecha_registro`) VALUES
(241, '', '', 'damian', '', 'damifmonti@gmail.com', 'tendran disponible lugar para esa fecha', '2021-11-26 15:39:23');

-- --------------------------------------------------------

--
-- Table structure for table `galeria`
--

CREATE TABLE `galeria` (
  `id` int(11) NOT NULL,
  `tittle` varchar(50) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  `categoria` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeria`
--

INSERT INTO `galeria` (`id`, `tittle`, `descripcion`, `url`, `categoria`) VALUES
(1, 'Cabaña 60mts', 'Planta Baja', 'img/portfolio/CAB_PB_60MTS.jpg', 'cab_60'),
(2, 'Cabaña 60mts', 'Planta Baja', 'img/portfolio/CAB_PB_60MTS_02.jpg', 'cab_60'),
(3, 'Cabaña 60mts', 'Planta Alta', 'img/portfolio/CAB_PA_60MTS.jpg', 'cab_60'),
(4, 'Cabaña 60mts', 'Planta Alta', 'img/portfolio/CAB_PA_60MTS_02.jpg', 'cab_60'),
(5, 'Cabaña 60mts', '', 'img/portfolio/cab_60mts_01.jpeg', 'cab_60'),
(6, 'Cabaña 60mts', '', 'img/portfolio/cab_12.jpeg', 'cab_60'),
(7, 'Cabaña 45mts', 'Planta Baja', 'img/portfolio/45mts/45_1.jpeg', 'cab_45_e'),
(8, 'Cabaña 45mts', 'Planta Baja', 'img/portfolio/45mts/45_2.jpeg', 'cab_45_e'),
(9, 'Cabaña 45mts', 'Planta Baja', 'img/portfolio/45mts/45_3.jpeg', 'cab_45_e'),
(10, 'Cabaña 45mts', 'Planta Baja', 'img/portfolio/45mts/45_4.jpeg', 'cab_45_e'),
(11, 'Cabaña 45mts', 'Planta Baja', 'img/portfolio/45mts/45_5.jpeg', 'cab_45_e'),
(12, 'Cabaña 45mts', 'Planta Baja', 'img/portfolio/45mts/45_6.jpeg', 'cab_45_e'),
(13, 'Cabaña 45mts', 'Planta Baja', 'img/portfolio/45mts/45_7.jpeg', 'cab_45_e'),
(14, 'Cabaña 45mts', 'Planta Alta', 'img/portfolio/45mts_superior/45sup_1.jpeg', 'cab_45_s'),
(15, 'Cabaña 45mts', 'Planta Alta', 'img/portfolio/45mts_superior/45sup_2.jpeg', 'cab_45_s'),
(16, 'Cabaña 45mts', 'Planta Alta', 'img/portfolio/45mts_superior/45sup_3.jpeg', 'cab_45_s'),
(17, 'Cabaña 45mts', 'Planta Alta', 'img/portfolio/45mts_superior/45sup_4.jpeg', 'cab_45_s'),
(18, 'Cabaña 45mts', 'Planta Alta', 'img/portfolio/45mts_superior/45sup_5.jpeg', 'cab_45_s'),
(19, 'Cabaña 45mts', 'Planta Alta', 'img/portfolio/45mts_superior/45sup_6.jpeg', 'cab_45_s'),
(20, 'Cabaña', '', 'img/portfolio/20211002/01.jpeg', 'cab_45_s'),
(21, 'Cabaña', '', 'img/portfolio/20211002/02.jpeg', 'cab_45_s'),
(22, 'Cabaña', '', 'img/portfolio/20211002/04.jpeg', 'cab_45_s'),
(23, 'Cabaña', '', 'img/portfolio/20211002/06.jpeg', 'cab_45_s'),
(24, 'Cabaña', '', 'img/portfolio/20211002/07.jpeg', 'cab_45_s'),
(25, 'Cabaña', '', 'img/portfolio/20211002/09.jpeg', 'cab_45_s'),
(26, 'Cabaña', '', 'img/portfolio/20211002/10.jpeg', 'cab_45_s'),
(27, 'Cabaña', '', 'img/portfolio/20211002/12.jpeg', 'cab_45_s'),
(28, 'Cabaña', '', 'img/portfolio/20211002/13.jpeg', 'cab_45_s'),
(29, 'Cabaña', '', 'img/portfolio/20211002/14.jpeg', 'cab_45_s'),
(30, 'Cabaña', '', 'img/portfolio/20211002/16.jpeg', 'cab_45_s'),
(31, 'Cabaña', '', 'img/portfolio/20211002/17.jpeg', 'cab_45_s'),
(32, 'Cabaña', '', 'img/portfolio/20211002/18.jpeg', 'cab_45_s'),
(33, 'Cabaña', '', 'img/portfolio/20211002/19.jpeg', 'cab_45_s'),
(34, 'Complejo', '', 'img/portfolio/complejo/9.jpeg', 'complejo'),
(35, 'Complejo', '', 'img/portfolio/complejo/10.jpeg', 'complejo'),
(36, 'Complejo', '', 'img/portfolio/complejo/11.jpeg', 'complejo'),
(37, 'Complejo', '', 'img/portfolio/complejo/12.jpeg', 'complejo'),
(38, 'Complejo', '', 'img/portfolio/complejo/13.jpeg', 'complejo'),
(39, 'Complejo', '', 'img/portfolio/complejo/14.jpeg', 'complejo'),
(40, 'Complejo', '', 'img/proyecto2020/pileta/pileta_04.JPG', 'complejo'),
(41, 'Complejo', '', 'img/proyecto2020/pileta/pileta_05.JPG', 'complejo'),
(42, 'Complejo', '', 'img/proyecto2020/pileta/pileta_07.JPG', 'complejo'),
(43, 'Complejo', '', 'img/proyecto2020/pileta/pileta_09.JPG', 'complejo'),
(44, 'Complejo', '', 'img/proyecto2020/pileta/pileta_10.JPG', 'complejo'),
(45, 'Complejo', '', 'img/portfolio/cab_08.jpeg', 'complejo'),
(46, 'Complejo', '', 'img/portfolio/cab_11.jpeg', 'complejo'),
(47, 'Complejo', '', 'img/portfolio/20211001/01.jpeg', 'complejo'),
(48, 'Complejo', '', 'img/portfolio/20211001/02.jpeg', 'complejo'),
(49, 'Complejo', '', 'img/portfolio/20211001/03.jpeg', 'complejo'),
(50, 'Complejo', '', 'img/portfolio/20211001/05.jpeg', 'complejo'),
(51, 'Complejo', '', 'img/portfolio/20211001/05.jpeg', 'complejo'),
(52, 'Complejo', '', 'img/portfolio/20211001/06.jpeg', 'complejo'),
(53, 'Complejo', '', 'img/portfolio/20211001/07.jpeg', 'complejo'),
(54, 'Complejo', '', 'img/portfolio/20211001/09.jpeg', 'complejo'),
(55, 'Complejo', '', 'img/portfolio/20211001/10.jpeg', 'complejo'),
(56, 'Complejo', '', 'img/portfolio/20211001/11.jpeg', 'complejo'),
(57, 'Complejo', '', 'img/portfolio/20211001/12.jpeg', 'complejo'),
(58, 'Complejo', '', 'img/portfolio/20211001/13.jpeg', 'complejo'),
(59, 'Complejo', '', 'img/portfolio/20211001/14.jpeg', 'complejo'),
(60, 'Complejo', '', 'img/portfolio/20211001/15.jpeg', 'complejo'),
(61, 'Complejo', '', 'img/portfolio/20211001/16.jpeg', 'complejo'),
(62, 'Complejo', '', 'img/portfolio/20211001/17.jpeg', 'complejo'),
(63, 'Complejo', '', 'img/portfolio/20211001/18.jpeg', 'complejo'),
(64, 'Complejo', '', 'img/portfolio/20211001/19.jpeg', 'complejo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
