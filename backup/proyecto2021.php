<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <title>La Lucia - La Naturaleza es suya, nosotros le damos el lugar</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="La Lucia, san clemente, " name="keywords" />
  <meta content="Cabañas en San Clemente del Tuyú. La Naturaleza es suya, nosotros le damos el lugar."
    name="description" />

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon" />
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon" />

  <!-- Google Fonts -->
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700"
    rel="stylesheet" />

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
  <link href="lib/animate/animate.min.css" rel="stylesheet" />
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet" />
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" />
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet" />

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet" />

  <!-- Google Tag Manager -->
  <script>
    (function (w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({ "gtm.start": new Date().getTime(), event: "gtm.js" });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != "dataLayer" ? "&l=" + l : "";
      j.async = true;
      j.src = "https://www.googletagmanager.com/gtm.js?id=" + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, "script", "dataLayer", "GTM-PJMWT4G");
  </script>
  <!-- End Google Tag Manager -->
</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJMWT4G" height="0" width="0"
      style="display: none; visibility: hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!--==========================
  Header
  ============================-->
  <header id="header">
    <?php include_once('views/headerView.php'); ?>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix" style="
        background: #f5f8fd url('img/proyecto-bg.jpeg') center top no-repeat;
      ">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center">
        <div class="col-md-8 intro-info order-md-first order-last">
          <h2 style="font-size: 3em; color: #fff">Proyecto 2021</h2>
          <h3 style="color: #fff">
            Luego de una larga espera, por razones de público conocimiento,
            contamos con nuestra <strong>Nueva Pileta :)</strong>
          </h3>
        </div>

        <div class="col-md-4 intro-img order-md-last order-first"></div>
      </div>
    </div>
  </section>
  <!-- #intro -->

  <main id="main">
    <section id="portfolio" class="section-bg">
      <div class="container">
        <header class="section-header">
          <h3 class="section-title">Detalle del Proyecto</h3>
        </header>

        <div class="row portfolio-container">
          <div class="col-lg-12 col-md-12 portfolio-item filter-app" style="background-color: #fff; padding: 5px 10px">
            <p>
              Estamos trabajando para ampliar nuestro complejo y contar con un
              total de <strong>ocho cabañas</strong>. Vamos a estar sumando
              una <strong>Pileta</strong> para todo el complejo con
              <strong>Solarium</strong>. Para los mas chicos contaremos con un
              <strong>Parque con juegos</strong>.
            </p>
            <p>
              Tendremos ademas un
              <strong>Quincho de uso común</strong> buscando generar un
              espacio donde todos nuestros huespedes puedan compartir un lugar
              social.
            </p>
            <p>
              Tenemos un claro objetivo: generar a todos nuestros huespedes un
              lugar de calidez en contacto con la naturaleza donde puedan
              relajarse y disfrutar de un merecido descanso.
            </p>
            <p>
              <u>Queremos que <strong>La Lucia</strong> sea
                <strong>el lugar</strong> al que siempre quieras volver.</u>
            </p>
          </div>
        </div>

        <div class="row portfolio-container">
          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_01.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_01.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>



          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_03.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_03.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_04.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_04.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_05.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_05.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_06.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_06.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_07.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_07.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_08.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_08.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_09.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_09.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_10.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_10.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_12.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_12.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_13.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_13.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/pileta/pileta_14.JPG" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Pileta</a></h4>
                <div>
                  <a href="img/proyecto2020/pileta/pileta_14.JPG" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/2021_01.jpeg" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Quincho</a></h4>
                <div>
                  <a href="img/proyecto2020/2021_01.jpeg" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/2021_02.jpeg" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Quincho</a></h4>
                <div>
                  <a href="img/proyecto2020/2021_02.jpeg" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/2021_03.jpeg" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Quincho</a></h4>
                <div>
                  <a href="img/proyecto2020/2021_03.jpeg" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>



          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/2020_01.jpeg" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Planos</a></h4>
                <div>
                  <a href="img/proyecto2020/2020_01.jpeg" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/2020_02.jpeg" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Planos</a></h4>
                <div>
                  <a href="img/proyecto2020/2020_02.jpeg" class="link-preview" data-lightbox="portfolio"
                    data-title="Web 3" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/2020_03.jpeg" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Planos</a></h4>
                <div>
                  <a href="img/proyecto2020/2020_03.jpeg" class="link-preview" data-lightbox="portfolio"
                    data-title="App 2" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/proyecto2020/2020_04.jpeg" class="img-fluid" alt="" />
              <div class="portfolio-info">
                <h4><a href="#">Planos</a></h4>
                <div>
                  <a href="img/proyecto2020/2020_04.jpeg" class="link-preview" data-lightbox="portfolio"
                    data-title="Card 2" title="Preview"><i class="ion ion-eye"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- #portfolio -->
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <?php include_once('views/footerView.php'); ?>  
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <a href="https://api.whatsapp.com/send?phone=541166050229&text=Hola!%20Me%20gustaria%20saber%20mas%20de%20las%20cabañas!"
    target="_blank" class="back-whatsapp"><i class="fa fa-whatsapp"></i> Contactanos</a>

  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

  <script>
    $(document).ready(function () {
      $("#btnEnviar").click(function (e) {
        var datos =
          "name=" +
          $("#name").val() +
          "&email=" +
          $("#email").val() +
          "&message=" +
          $("#message").val();

        jQuery.ajax({
          url: "enviarmail.php",
          data: datos,
          type: "POST",
          success: function (data) {
            $("#sendmessage").show();

            setTimeout(function () {
              $("#sendmessage").fadeOut(1000);
            }, 2000);

            $("#name").val("");
            $("#email").val("");
            $("#message").val("");
          },
          error: function () { },
        });
      });
    });
  </script>
</body>

</html>